import React from 'react';
import NavItem from './NavItem';

const SubNavigation = () => (
  <nav className="sub-nav">
    <ul>
      <NavItem to='/dashboard'>Dashboard</NavItem>
      <NavItem to='/feedback'>Feedback</NavItem>
      <NavItem to='/assign'>Assign</NavItem>
      <NavItem to='/upload'>Upload</NavItem>
      <NavItem to='/trending'>Trending</NavItem>
    </ul>
  </nav>
);

export default SubNavigation;
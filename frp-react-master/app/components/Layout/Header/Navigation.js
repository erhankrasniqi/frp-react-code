import React from 'react';
import NavItem from './NavItem';

const Navigation = () => (
    // <div className="navbar navbar-default labelmenu">
    //     <div className="container">
    //         <div className="navbar-collapse app-menu">
    //             <ul className="nav navbar-nav lblmenu float-left">
    //                 <NavItem to='/dashboard'>Dashboard</NavItem>
    //                 <NavItem to='/feedback'>Feedback</NavItem>
    //                 <NavItem to='/assign'>Assign</NavItem>
    //                 <NavItem to='/upload'>Upload</NavItem>
    //                 <NavItem to='/trending'>Trending</NavItem>
    //             </ul>
    //         </div>
    //     </div>
    // </div>
    <nav className="top-nav">
      <ul>
        <NavItem to='/home'>Home</NavItem>
        <NavItem to='/heavy-rotation'>Heavy Rotation</NavItem>
        <NavItem to='/label'>Labels</NavItem>
        <NavItem to='/about'>About</NavItem>
        <NavItem to='/contact'>Contact</NavItem>
      </ul>
    </nav>
);

export default Navigation;
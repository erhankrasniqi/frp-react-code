import React from 'react'
import { useTheme } from '../../../utils/providers/themeProvider';
import styled, { withTheme } from 'styled-components';
import { buttonBackgroundColor, buttonTextColor } from '../../../utils/theme';

function ThemeSwitcher(props) {
  const themeToggle = useTheme();

  const Button = styled.button`
    background: ${buttonBackgroundColor};
    border: none;
    border-radius: 0.3em;
    box-shadow: none;
    color: ${buttonTextColor};
    cursor: pointer;
    font-size: 1em;
    padding: 0.5em 1em;
  `;
  return (
    <div>
      {/* <Button onClick={() => themeToggle.toggle()}>
        {props.theme.mode}
      </Button> */}
      <button onClick={() => themeToggle.toggle()} className="icon-button"><svg width="9" height="12" viewBox="0 0 9 12"><path fill="var(--theme-main-color)" d="M9 11.195A6 6 0 1 1 6 0c1.095 0 2.12.291 3 .805a5.987 5.987 0 0 0-2.999 5.194c0 2.22 1.206 4.16 3 5.196" opacity={props.theme.mode === 'dark' ? '1' : '0.2'}></path></svg></button>
      <button onClick={() => themeToggle.toggle()} className="icon-button"><svg width="16" height="16" viewBox="0 0 16 16"><path fill="var(--theme-main-color)" d="M8 4.08a3.92 3.92 0 1 1 0 7.84 3.92 3.92 0 0 1 0-7.84zm-.603-.979V0h1.207v3.101H7.397zm0 12.899v-3.101h1.207V16H7.397zm5.502-7.397V7.396H16v1.207h-3.101zM0 8.603V7.396h3.101v1.207H0zM14.084 2.77L11.89 4.963l-.854-.854 2.193-2.193.854.854zM1.916 13.23l2.194-2.193.853.854-2.193 2.192-.854-.853zm3.047-9.12l-.853.853L1.916 2.77l.854-.854L4.963 4.11zm6.074 7.78l.854-.853 2.193 2.193-.854.853-2.193-2.192z" opacity={props.theme.mode === 'light' ? '1' : '0.2'}></path></svg></button>
    </div>
  )
}

export default withTheme(ThemeSwitcher);

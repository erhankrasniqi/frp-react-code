import React from 'react';
import MainLogo from '../../../images/logo-3-d-92-d-322.png';
import MainLogoLightVersion from '../../../images/logo-light-version.png';
import Navigation from './Navigation';
import SearchForm from './SarchForm';
import { NavLink } from 'react-router-dom';
import tokenProvider from '../../../utils/providers/tokenProvider';
import { Subscription } from 'rxjs';
import ThemeSwitcher from './ThemeSwitcher';
import { withTheme } from 'styled-components';

class Header extends React.Component {
  subscription = new Subscription();

  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: false,
      showMenu: false
    };
  }

  async componentDidMount() {
    this.subscription = tokenProvider.checkAuth().subscribe(res => {
      this.setState({ isAuthenticated: res.isAuthenticated });
    });
    await this.setState({ isAuthenticated: tokenProvider.isAuthenticated() });
  }

  componentWillUnmount() {
    this.subscription.unsubscribe();
  }


  render() {
    const logoutBtn = () => {
      tokenProvider.logout();
    }
    const toggleMenu = () => {
      this.setState({
        showMenu: !this.state.showMenu
      })
    }

    return (
      <header id="header">
        <div className="container">
          <div className="row">
            <div className="col-sm-3">
              <img  src={this.props.theme.mode === 'dark' ? MainLogo : MainLogoLightVersion} className="logo" />
              <button className={"hamburger hamburger--squeeze d-inline-block d-sm-none float-right mt-4 " + (this.state.showMenu ? "is-active" : "")} onClick={toggleMenu} type="button">
                <span className="hamburger-box">
                  <span className="hamburger-inner"></span>
                </span>
              </button>
            </div>
            <div className={"col-sm-9 collapse d-sm-block" + (this.state.showMenu ? " show" : "")}>
              <div className="row">
                <div className="col-sm-8 text-center">
                  <Navigation />
                  <div className="row">
                    <div className="col-9 col-sm-8 col-md-9 col-lg-10 pr-1">
                      <SearchForm />
                    </div>
                    <div className="col-3 col-sm-4 col-md-3 col-lg-2 pl-0 theme-switcher">
                      <ThemeSwitcher />
                    </div>
                  </div>
                  {/* <div style={{width: '84%', display: 'inline-block'}}>
                  </div>
                  <div className="theme-switcher" style={{width: '15%', display: 'inline-block'}}>
                  </div> */}
                </div>
                <div className="col-sm-4 text-center text-sm-right pt-sm-4 my-3">
                  {
                    !this.state.isAuthenticated ? (
                      <span>
                        <NavLink className="header-btn" to="/auth/login">SIGN IN</NavLink>
                        <NavLink className="header-btn" to="/auth/signup">SIGN UP</NavLink>
                      </span>
                    ) : (
                      <a onClick={logoutBtn} className="header-btn">SIGN OUT</a>
                    )
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default withTheme(Header);
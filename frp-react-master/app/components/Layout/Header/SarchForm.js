import React from 'react'
import SearchIcon from '../../../images/search-1.png';

export default function SarchForm() {
  return (
    <form className="search-form">
      <input type="search" placeholder="Search" />
      <img src={SearchIcon} className="search-icon" />
    </form>
  )
}
import React from 'react'
import { withTheme } from 'styled-components';

import Logo1 from '../../../images/logo-1.png';
import Logo2 from '../../../images/logo-2.png';
import Logo3 from '../../../images/logo-3.png';
import Logo4 from '../../../images/logo-4.png';
import Logo5 from '../../../images/logo-5.png';
import MainLogo from '../../../images/frp-logo.png';

import Facebook from '../../../images/facebook.png';
import Instagram from '../../../images/instagram.png';
import Twitter from '../../../images/twitter.png';
import Google from '../../../images/google.png';

import LightTheme from '../../../styles/lightColorTheme.css';
import DarkTheme from '../../../styles/darkColorTheme.css';

function Footer(props) {
    return (
      <div id="footer">
        <div className="container-fluid pt-5 pb-5" style={{backgroundColor: 'var(--theme-main-color)'}}>
          <div className="container mt-5 mb-5">
            <div className="row">
              <div className="col-md-3">
                <img className="mb-5" src={MainLogo} alt=""/>
                <p>Connecticut Office/Web Services <br />
                  P.O. Box 241 Norwalk, CT 06856</p>
                <p>347-449-5129</p>

                <div className="social-icons">
                  <a href="#"><img src={Facebook} alt="Facebook"/></a>
                  <a href="#"><img src={Instagram} alt="Instagram"/></a>
                  <a href="#"><img src={Twitter} alt="Twitter"/></a>
                  <a href="#"><img src={Google} alt="Facebook"/></a>
                </div>

              </div>
              <div className="col-md-3">
                <div className="footer-menu mb-5">
                  <h2><a href="#">Heavy Rotation</a></h2>
                  <ul>
                    <li><a href="#">Promo Package</a></li>
                  </ul>
                </div>
                <div className="footer-menu">
                  <h2><a href="#">Frp Apps</a></h2>
                  <ul>
                    <li><a href="#">Frp LIVE</a></li>
                    <li><a href="#">Spins</a></li>
                    <li><a href="#">WC LIVE</a></li>
                  </ul>
                </div>
              </div>
              <div className="col-md-3">
                <div className="footer-menu mb-5">
                  <h2><a href="#">Billboard top 100</a></h2>
                </div>
                <div className="footer-menu mb-5">
                  <h2><a href="#">FRP TV</a></h2>
                </div>
                <div className="footer-menu">
                  <h2><a href="#">Labels</a></h2>
                </div>
              </div>
              <div className="col-md-3">
                <div className="footer-menu mb-5">
                  <h2><a href="#">About</a></h2>
                  <ul>
                    <li><a href="#">The pool</a></li>
                    <li><a href="#">LABEL BENEFITS</a></li>
                    <li><a href="#">Pricing</a></li>
                  </ul>
                </div>
                <div className="footer-menu">
                  <h2><a href="#">Contact</a></h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container-fluid text-center pt-5 pb-5" style={{backgroundColor: '#0d1c2d'}}>
          <div className="row">
            <div className="col"><a href="https://www.franchiserecordpool.com" target="_blank"><img src={Logo1} alt="FRP TV"/></a></div>
            <div className="col"><a href="https://www.franchiserecordpool.com" target="_blank"><img src={Logo2} alt="DJ ON LIVE"/></a></div>
            <div className="col"><a href="https://www.franchiserecordpool.com" target="_blank"><img src={Logo3} alt="EVENT ON LIVE"/></a></div>
            <div className="col"><a href="https://www.franchiserecordpool.com" target="_blank"><img src={Logo4} alt="WC LIVE"/></a></div>
            <div className="col"><a href="https://www.franchiserecordpool.com" target="_blank"><img src={Logo5} alt="SPINS"/></a></div>
          </div>
        </div>
        {
          props.theme.mode === 'light' ?
          <style  dangerouslySetInnerHTML={{__html: `
            :root {
              --theme-main-color: #0e1521;
              --theme-second-color: #fff;
              --theme-muffled-color: #aeb0bd;
              --theme-changed-color: #aeb0bd;
              --theme-changed-opacity: 1;
              --theme-background-light-color: rgba(13, 28, 45, 0.06);
              --theme-background-transparent-color: rgba(255, 255, 255, 0.7);
              --theme-background-highttransparent-color: rgba(255, 255, 255, 0.3);
              --theme-background-color: #fff;
              --theme-background-changed-color: #d5d6d7;
              --theme-error-color: rgba(255, 0, 0, 0.5);
              --theme-panel-background: rgba(255, 255, 255, 0.3);
            }
          `}} />
           :
          <style  dangerouslySetInnerHTML={{__html: `
            :root {
              --theme-main-color: #fff;
              --theme-second-color: #0d1c2c;
              --theme-changed-opacity: 0.4;
              --theme-changed-color: #bb1d7e;
              --theme-muffled-color: rgba(255, 255, 255, 0.3);
              --theme-background-light-color: rgba(255, 255, 255, 0.13);
              --theme-background-transparent-color: rgba(13, 28, 45, 0.5);
              --theme-background-highttransparent-color: rgba(13, 28, 45, 0.5);
              --theme-background-color: #0d1c2d;
              --theme-background-changed-color: #213044;
              --theme-error-color: rgba(255, 0, 0, 0.8);
              --theme-panel-background: rgba(0, 0, 0, 0.3);
            }
          `}} />
        }
        <link rel="stylesheet" type="text/css" href={(props.theme.mode === 'light' ? '/styles/lightColorTheme.css' : '/styles/darkColorTheme.css')} />
      </div>
    )
}

export default withTheme(Footer);
import React from 'react';
import { NavLink } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

import A from './A';
import Img from './Img';
import NavBar from './NavBar';
import HeaderLink from './HeaderLink';
import Banner from './banner.jpg';
import messages from './messages';

function Header() {
  return (
    <div>
      <A href="https://www.reactboilerplate.com/">
        <Img src={Banner} alt="react-boilerplate - Logo" />
      </A>
      <ul class="nav nav-tabs mt-3 mb-3">
        <li class="nav-item">
          <NavLink to="/home" className="nav-link"><FormattedMessage {...messages.home} /></NavLink>
        </li>
        <li class="nav-item">
          <NavLink to="/features" className="nav-link"><FormattedMessage {...messages.features} /></NavLink>
        </li>
      </ul>
      {/* <NavBar>
        <HeaderLink to="/">
          <FormattedMessage {...messages.home} />
        </HeaderLink>
        <HeaderLink to="/features">
          <FormattedMessage {...messages.features} />
        </HeaderLink>
      </NavBar> */}
    </div>
  );
}

export default Header;

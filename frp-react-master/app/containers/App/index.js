import React, { memo } from 'react';
import { Helmet } from 'react-helmet';
import { Switch, Route, Redirect, BrowserRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Header from 'components/Layout/Header/Header';

import GlobalStyle from '../../global-styles';
import AuthPage from '../AuthPage';
import LabelPage from '../LabelPage';
import Footer from '../../components/Layout/Footer';
import PrivateRoute from '../../utils/routes/privateRoute';
import { useTheme } from '../../utils/providers/themeProvider';
import { withTheme } from 'styled-components';


export function App(props) {

  const themeToggle = useTheme();

  return (
    <div className={'app-wrapper ' + props.theme.mode}>
      <Helmet
        titleTemplate="%s - Franchise Record Pool"
        defaultTitle="Franchise Record Pool"
      >
        <meta name="description" content="Franchise Record Pool is the largest and most advanced record pool on the web. Serving over 3000 tracks per month in 19 genres along with music videos, we offer additional features such as a web streaming TV Network showcasing our DJs and Artist, an App and customer software called Tracker that allows for downloading music when on the go and artist receiving live play alerts and last but not least the most up to date features for navigation and feedback reporting. Franchise Record Pool is a one stop power house for music promotion." />
      </Helmet>
      <Header />
        <Switch>
          <Redirect exact path="/" to="/auth"></Redirect>
          <Route path="/auth" component={AuthPage} />
          <PrivateRoute path='/label' component={LabelPage} />
          {/* <Route path="/app" component={AppPage} /> */}
          <Route path="" component={NotFoundPage} />
        </Switch>
      <Footer />
      <GlobalStyle />
    </div>
  );
}


const mapStateToProps = createStructuredSelector({
  // headerImage: makeSelectHeaderImage()
});

export function mapDispatchToProps(dispatch) {
  return { };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  withTheme,
  memo,
)(App);

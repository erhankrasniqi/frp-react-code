import { Subject } from 'rxjs';

const subject = new Subject();
export var title = '';

export const setTitle = (t) => {
  title = t;
  subject.next(title);
}

export const getTitle = () => subject.asObservable();

import React, { Component } from 'react'
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { loginUserAction } from '../../../utils/actions/authActions';
import tokenProvider from '../../../utils/providers/tokenProvider';
import { Helmet } from "react-helmet";
import { setTitle } from '../authPageSevice';

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'Sign in'
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleSubmit(event) {
    event.preventDefault();
    
    let username = event.target.username.value;
    let password = event.target.password.value;

    if(!username || !password) {
      alert('Please enter username and password');
      return;
    }

    const data =  {
      username,
      password
    };

    this.props.dispatch(loginUserAction(data));

    setTimeout(() => {
    }, 1500);
  }

  componentDidMount() {
    setTitle(this.state.title);
  }

  render() {
    let isSuccess, message;

    if (this.props.response.login.hasOwnProperty('response')) {
      if(this.props.response.login.response) {
        isSuccess = this.props.response.login.response.success;
        message = this.props.response.login.response.message;
        
        if (isSuccess) {
          tokenProvider.setToken(this.props.response.login.response.data.token);
          tokenProvider.setUser(this.props.response.login.response.data.user);
        }
      }
    }

    return (
      <div className="col-sm-6 col-md-5 col-lg-3 text-center">
        <Helmet>
            <title>{this.state.title}</title>
        </Helmet>

        <form onSubmit={this.handleSubmit}>
          {!isSuccess ? <div className="text-white">{message}</div> : <Redirect to='/label/dashboard' />}
          <div className="form-group form-label-group pt-3">
            <input type="text" placeholder="Login" name="username" className="form-control" id="username" />
            <label htmlFor="username">Login</label>
          </div>
          <div className="form-group form-label-group pt-3">
            <input type="password" placeholder="Password" name="password" className="form-control" id="password" />
            <label htmlFor="password">Password</label>
          </div>
          <div className="mt-4 pt-4">
            <button type="submit" className="btn btn-block btn-outline-light btn-round">Sign in</button>
            <button type="submit" className="btn btn-clear btn-link btn-block">Forgot password</button>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (response) => ({response});

export default connect(mapStateToProps)(LoginPage);
import React from 'react'
import SignupPage from './SignupPage';
import LoginPage from './LoginPage';
import { Switch, Route, Redirect, NavLink } from 'react-router-dom';
import tokenProvider from '../../utils/providers/tokenProvider';
import { getTitle, title } from './authPageSevice';
import { Subscription } from 'rxjs';

export default class AuthPage extends React.Component {
  titleSubscription = new Subscription();

  constructor(props) {
    super(props);
    
    
    this.state = {
      isAuthenticated: false,
      title: ''
    };
  }

  async componentDidMount() {
    this.subscription = tokenProvider.checkAuth().subscribe(res => {
      this.setState({ isAuthenticated: res.isAuthenticated });
    });

    this.titleSubscription = getTitle().subscribe(title => this.setState({title}));

    await this.setState({ isAuthenticated: tokenProvider.isAuthenticated(), title: title })
  }

  componentWillUnmount() {
    this.subscription.unsubscribe();
    this.titleSubscription.unsubscribe();
  }
  

  render() {
    return !this.state.isAuthenticated ? (
      <div className="wrapper container-fluid pt-5">
        <h1 className="large-title text-center mt-0 mb-3" style={{color: 'var(--theme-main-color)'}}>{this.state.title}</h1>
        <div className="row justify-content-center">
          <div className="col-sm-6 col-md-5 col-lg-3 text-center">
            <ul className="auth-menu mb-4">
              <li>
                <NavLink to="/auth/login">Sign in</NavLink>
              </li>
              <li>
                <NavLink to="/auth/signup">Sign up</NavLink>
              </li>
            </ul>
          </div>
        </div>
        <div className="row justify-content-center">
          <Switch>
            <Redirect exact path="/auth" to="/auth/login"></Redirect>
            <Route path="/auth/signup" component={SignupPage} />
            <Route path="/auth/login" component={LoginPage} />
          </Switch>
        </div>
      </div>
    ) : <Redirect to='/label/dashboard' />
  }
}

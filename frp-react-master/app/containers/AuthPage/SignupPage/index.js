import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import { setTitle } from '../authPageSevice';
import httpClient from '../../../utils/providers/httpClient';
import {
  Redirect
} from 'react-router-dom'

export default class SignupPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: 'Sign up',
      registered: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    
    let fullName = event.target.fullName.value;
    let email = event.target.email.value;
    let phone = event.target.phone.value;
    let username = event.target.username.value;
    let password = event.target.password.value;
    let confirmPassword = event.target.confirmPassword.value;

    if(!username || !password) {
      alert('Please enter username and password');
      return;
    }

    if(password !== confirmPassword) {
      alert('Please enter your password twice');
      return;
    }

    const data =  {
      fullName,
      email,
      phone,
      username,
      password
    };

    // console.log(data);

    httpClient.post('api/auth/register', data).then((res) => {
      if(res.data.success) {
        alert('Registration has been completed successfully!');
        this.setState({
          registered: true
        });
      } else {
        alert(res.data.message)
      }
    }).catch(err => {
      console.error(err);
    });
  }

  componentDidMount() {
    setTitle(this.state.title);
  }

  render() {
    return this.state.registered ? <Redirect to='/auth/login' /> :
    (
      <div className="col-sm-8 col-md-7 col-lg-4 text-center">
        <Helmet>
          <title>{this.state.title}</title>
        </Helmet>

        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group form-label-group pt-3">
                <input type="text" placeholder="Full Name" name="fullName" className="form-control" id="fullName" />
                <label htmlFor="fullName">Full Name</label>
              </div>
              <div className="form-group form-label-group pt-3">
                <input type="email" placeholder="Email" name="email" className="form-control" id="email" />
                <label htmlFor="email">Email</label>
              </div>
              <div className="form-group form-label-group pt-3">
                <input type="phone" placeholder="Phone" name="phone" className="form-control" id="phone" />
                <label htmlFor="phone">Phone</label>
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group form-label-group pt-3">
                <input type="text" placeholder="Login" name="username" className="form-control" id="username" />
                <label htmlFor="username">Login</label>
              </div>
              <div className="form-group form-label-group pt-3">
                <input type="password" placeholder="Password" name="password" className="form-control" id="password" />
                <label htmlFor="password">Password</label>
              </div>
              <div className="form-group form-label-group pt-3">
                <input type="password" placeholder="Confirm password" name="confirmPassword" className="form-control" id="confirmPassword" />
                <label htmlFor="confirmPassword">Confirm password</label>
              </div>
            </div>
          </div>
          <div className="mt-4 pt-4">
            <button type="submit" className="btn btn-block btn-outline-light btn-round">Continue</button>
          </div>
        </form>
      </div>
    )
  }
}

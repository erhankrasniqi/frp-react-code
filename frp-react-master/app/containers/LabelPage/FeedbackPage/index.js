import React, { Component } from 'react'
import MusicIcon from '../../../images/music-player.png';


import Feedback from '../DashboardPage/Feedback';
import GlobalDownloadCount from '../DashboardPage/GlobalDownloadCount';
import CountryDownloadCount from '../DashboardPage/CountryDownloadCount';
import FeedbackTracks from './FeedbackTracks';
import httpClient from '../../../utils/providers/httpClient';

export default class FeedbackPage extends Component {

  state = {
    trackId: null,
    trackName: null,
    counts: null,
    downloadMapMarkers: null,
    selectedCountry: null,
    selectedCity: null
  }

  getLabelTrack = () => {
    httpClient.get(`/api/dashboard/trackdata/${this.state.trackId}`)
    .then((res) => {
      if(res.data.success === true) {
        this.setState({counts: res.data.data });
      }
    })
  }

  getDownloadMapMarkers = () => {
    // Get downloads by country
    httpClient.get(`/api/dashboard/downloadscountryregion/${this.state.trackId}`)
    .then((res) => {
      if(res.data.success === true) {
        this.setState({
          downloadMapMarkers: res.data.data,
          trackName: `${res.data.data.artist} ${res.data.data.name}`
        });
      }
    })
  }

  onTrackChange = (track) => {
    this.setState({
      trackId: track.id,
      selectedCountry: null,
      selectedCity: null
    })
    setTimeout(() => {
      this.initTrack();
    }, 500);
  }

  componentWillMount() {
    // this.initTrack();
  }

  initTrack() {
    this.getLabelTrack();
    this.getDownloadMapMarkers();
  }

  onCountrySelect = (data) => {
    this.setState({
      selectedCountry: data.country_code
    });
  }


  render() {

    return (
      <div style={{minHeight: '1000px'}}>
        <div className="label-bg feedback-bg"></div>
        <FeedbackTracks onTrackChange={this.onTrackChange}></FeedbackTracks>
        {
          !this.state.trackId ? ("") : (
            <div>
              <Feedback className="color-purple" trackId={this.state.trackId} counts={this.state.counts} onCountrySelect={this.onCountrySelect} />
              <GlobalDownloadCount trackId={this.state.trackId} downloadMapMarkers={this.state.downloadMapMarkers} onCountrySelect={this.onCountrySelect} />
              <CountryDownloadCount trackId={this.state.trackId} selectedCountry={this.state.selectedCountry} onCitySelect={this.onCitySelect} />
            </div>
          )
        }
      </div>
    )
  }
}

import React, { Component } from 'react'
import MusicIcon from '../../../images/music-player.png';
import StarIcon from '../../../images/star.png';
import DownloadIcon from '../../../images/download.png';
import ListensIcon from '../../../images/headphones.png';
import paginate from '../../../utils/paginate';
import httpClient from '../../../utils/providers/httpClient';

export default class FeedbackTracks extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    page: 1,
    pageSize: 12,
    totalItems: 0,
    items: [],
    selectedItem: null,
    trackId: null,
    paginate: null,
  }

  getFeedbackTracks = () => {
    httpClient.get(`/api/track/page/${this.state.page}/size/${this.state.pageSize}`)
    .then((res) => {
      if(res.data.success === true) {
        this.setState({items: res.data.data.items, totalItems: res.data.data.totalItems });
        this.generatePagination();
      }
    })
  }

  async componentWillMount() {
    this.getFeedbackTracks();
  }

  async generatePagination() {
    const generatedPaginate = paginate(this.state.totalItems, this.state.page, this.state.pageSize, 5);
    await this.setState({
      paginate: generatedPaginate
    })
    console.log(generatedPaginate);
  }

  onPageChange = async (e) => {
    e.preventDefault();
    const page = e.target.dataset.page;
    if(page === this.state.page) {
      return;
    }
    const loop = Math.ceil(this.state.records / this.state.total);
    if(page === 0 || page > loop) {
      return;
    }
    await this.setState({page: parseInt(page)});
    this.getFeedbackTracks();
  }

  onTrackChange = async (e) => {
    e.preventDefault();
    const trackId = e.target.dataset.track;
    if(trackId) {
      this.props.onTrackChange({id: parseInt(trackId)});
      await this.setState({
        trackId: parseInt(trackId)
      })

      const selectedItems = this.state.items.filter(item => item.track_id === parseInt(trackId));
      if(selectedItems && selectedItems.length > 0) {
        await this.setState({
          selectedItem: selectedItems[0]
        })
      }
    }
  }

  render() {

    const nagiation = () => {
      if(!this.state.paginate) {
        return '';
      }
      const currentPage = this.state.page;
      let pages = [];
      for (let i = 0; i < this.state.paginate.pages.length; i++) {
        const page = this.state.paginate.pages[i];
        pages.push(
          <li key={i} className={page == currentPage ? "page-item active" : "page-item"}>
            <a className="page-link" href="#" data-page={page} onClick={this.onPageChange}>{page}</a>
          </li>
        )
      }
      return pages;
    }

    const items = () => {
      const pages = [];
      this.state.items.forEach((item, i) => {
        pages.push(
          <div key={i} className="col-md-3 mb-4">
            <a className={this.state.trackId === item.track_id ? 'feedback-item active' : 'feedback-item'} data-track={item.track_id} onClick={this.onTrackChange}>
              <div className="track" data-track={item.track_id} >
                <img src={MusicIcon} alt="Musical Note Icon" className="float-left mr-3 mt-1" data-track={item.track_id}  />
                <p className="track-artist" data-track={item.track_id} >{item.artist}</p>
                <p className="track-title" data-track={item.track_id} >{item.song}</p>
              </div>
              <ul className="feedback-item-footer" data-track={item.track_id} >
                <li>
                  <img src={StarIcon} alt="Star Icon" className="mr-2" />
                  {item.rating}
                </li>
                <li>
                  <img src={DownloadIcon} alt="Download Icon" className="mr-2" />
                  {item.downloads}
                </li>
                <li>
                  <img src={ListensIcon} alt="Listens Icon" className="mr-2" />
                  {item.listens}
                </li>
              </ul>
            </a>
          </div>
        )
      });
      return pages;
    }

    return (
      <div className="container text-white">
        <div className="row row-eq-height pt-5">
          {items()}
        </div>

        <div className="row mt-5 mb-5 frp-pagination">
          <div className="col-4 text-left">
            <button className="btn-left" data-page={parseInt(this.state.page) - 1} onClick={this.onPageChange}>&laquo;</button>
          </div>
          <div className="col-4 text-center">
            <nav aria-label="Page navigation">
              <ul className="pagination justify-content-center">
                {nagiation()}
              </ul>
            </nav>
          </div>
          <div className="col-4 text-right">
            <button className="btn-right" data-page={parseInt(this.state.page) + 1} onClick={this.onPageChange}>&raquo;</button>
          </div>
        </div>

        {
          this.state.selectedItem ? (
            <div className="row pt-5 mt-5 justify-content-md-center">
              <div className="col-md-7">
                <div className="feedback-item active">
                  <div className="track text-center">
                    <p style={{fontSize: '14px'}}>
                      <img src={MusicIcon} alt="Musical Note Icon" className="mr-3" />
                      {this.state.selectedItem.artist} - <span style={{fontWeight: 'bold'}}>{this.state.selectedItem.song}</span>
                    </p>
                  </div>
                  <ul className="feedback-item-footer big-block">
                    <li>
                      Rating
                      <span style={{display: 'block', fontWeight: 'bold'}}>{this.state.selectedItem.rating}</span>
                    </li>
                    <li>
                      Download Count
                      <span style={{display: 'block', fontWeight: 'bold'}}>{this.state.selectedItem.downloads}</span>
                    </li>
                    <li>
                      Listen Count
                      <span style={{display: 'block', fontWeight: 'bold'}}>{this.state.selectedItem.listens}</span>
                    </li>
                  </ul>ß
                </div>
              </div>
            </div>
          ) : ''
        }
      </div>
    )
  }
}

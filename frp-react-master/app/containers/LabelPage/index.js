import React, { memo } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { makeSelectHeaderImage } from '../App/selectors';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import DashboardPage from './DashboardPage'
import NavItem from 'components/Layout/Header/NavItem';
import UploadPage from './UploadPage';
import TrendingPage from './TrendingPage';
import FeedbackPage from './FeedbackPage';
import AssignPage from './AssignPage';


export function LabelPage({
  headerImage
}) {
  console.log(headerImage);
  return (
    <div style={{position: 'relative', overflow: 'hidden'}}>
      <div className="row justify-content-md-center" style={{zIndex: '10', position: 'relative'}}>
        <div className="col-md-6 text-center">
          <h1 style={{color: 'var(--theme-main-color)', fontSize: '64px', fontWeight: '900', marginTop: '32px'}}>Labels</h1>
          <nav className="sub-nav">
            <ul>
              <NavItem to='/label/dashboard'>Dashboard</NavItem>
              <NavItem to='/label/feedback'>Feedback</NavItem>
              <NavItem to='/label/assign'>Assign</NavItem>
              <NavItem to='/label/upload'>Upload</NavItem>
              <NavItem to='/label/trending'>Trending</NavItem>
            </ul>
          </nav>
        </div>
      </div>
      <Switch>
        <Redirect exact path="/label" to="/label/dashboard"></Redirect>
        <Route exact path="/label/dashboard" component={DashboardPage} />
        <Route exact path="/label/feedback" component={FeedbackPage} />
        <Route exact path="/label/assign" component={AssignPage} />
        <Route exact path="/label/upload" component={UploadPage} />
        <Route exact path="/label/trending" component={TrendingPage} />
      </Switch>
    </div>
  );
}


const mapStateToProps = createStructuredSelector({
  headerImage: makeSelectHeaderImage()
});

export function mapDispatchToProps(dispatch) {
  return { };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(LabelPage);

import React, { Component } from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4maps from "@amcharts/amcharts4/maps";
import {HorizontalBar} from 'react-chartjs-2';
import httpClient from '../../../utils/providers/httpClient';

am4core.useTheme(am4themes_animated);

class CountryDownloadCount extends Component {

  constructor(props) {
    super(props);
    this.state = {
      location: null,
      chart: null,
      barChart: null,
      markers: null,
      topTenCharts: null,
      allGrid: null
    }

  }

  componentWillUnmount() {
    if (this.state.chart) {
      this.state.chart.dispose();
    }
  }

  countryMaps = {
    "AL": [ "albaniaLow" ],
    "DZ": [ "algeriaLow" ],
    "AD": [ "andorraLow" ],
    "AO": [ "angolaLow" ],
    "AR": [ "argentinaLow" ],
    "AM": [ "armeniaLow" ],
    "AU": [ "australiaLow" ],
    "AT": [ "austriaLow" ],
    "AZ": [ "azerbaijanLow" ],
    "BH": [ "bahrainLow" ],
    "BD": [ "bangladeshLow" ],
    "BY": [ "belarusLow" ],
    "BE": [ "belgiumLow" ],
    "BZ": [ "belizeLow" ],
    "BM": [ "bermudaLow" ],
    "BT": [ "bhutanLow" ],
    "BO": [ "boliviaLow" ],
    "BW": [ "botswanaLow" ],
    "BR": [ "brazilLow" ],
    "BN": [ "bruneiDarussalamLow" ],
    "BG": [ "bulgariaLow" ],
    "BF": [ "burkinaFasoLow" ],
    "BI": [ "burundiLow" ],
    "KH": [ "cambodiaLow" ],
    "CM": [ "cameroonLow" ],
    "CA": [ "canandaLow" ],
    "CV": [ "capeVerdeLow" ],
    "CF": [ "centralAfricanRepublicLow" ],
    "TD": [ "chadLow" ],
    "CL": [ "chileLow" ],
    "CN": [ "chinaLow" ],
    "CO": [ "colombiaLow" ],
    "CD": [ "congoDRLow" ],
    "CG": [ "congoLow" ],
    "CR": [ "costaRicaLow" ],
    "HR": [ "croatiaLow" ],
    "CZ": [ "czechRepublicLow" ],
    "DK": [ "denmarkLow" ],
    "DJ": [ "djiboutiLow" ],
    "DO": [ "dominicanRepublicLow" ],
    "EC": [ "ecuadorLow" ],
    "EG": [ "egyptLow" ],
    "SV": [ "elSalvadorLow" ],
    "EE": [ "estoniaLow" ],
    "SZ": [ "eswatiniLow" ],
    "FO": [ "faroeIslandsLow" ],
    "FI": [ "finlandLow" ],
    "FR": [ "franceLow" ],
    "GF": [ "frenchGuianaLow" ],
    "GE": [ "georgiaLow" ],
    "DE": [ "germanyLow" ],
    "GR": [ "greeceLow" ],
    "XK": [ "kosovoLow" ],
    "GL": [ "greenlandLow" ],
    "GN": [ "guineaLow" ],
    "HN": [ "hondurasLow" ],
    "HK": [ "hongKongLow" ],
    "HU": [ "hungaryLow" ],
    "IS": [ "icelandLow" ],
    "IN": [ "indiaLow" ],
    "GB": [ "ukLow" ],
    "IE": [ "irelandLow" ],
    "IL": [ "israelLow" ],
    "PS": [ "palestineLow" ],
    "PT": [ "portugalLow" ],
    "MT": [ "italyLow" ],
    "SM": [ "italyLow" ],
    "VA": [ "italyLow" ],
    "IT": [ "italyLow" ],
    "JP": [ "japanLow" ],
    "MX": [ "mexicoLow" ],
    "RU": [ "russiaCrimeaLow" ],
    "KR": [ "southKoreaLow" ],
    "ES": [ "spainLow" ],
    "US": [ "usaAlbersLow" ]
  };

  // componentDidMount() {
  //   this.setState({
  //     markers: this.markers,
  //     topTenCharts: this.topTenCharts,
  //     allGrid: this.allGrid
  //   })
  // }

  async getData() {
    this.setState({loading: true});
    
    await httpClient.get(`/api/dashboard/markers/${this.props.trackId}/location/${this.state.location}`)
    .then((res) => {
      if(res.data.success === true) {
        this.setState({markers: res.data.data });
      }
    })
    await httpClient.get(`/api/dashboard/chart/${this.props.trackId}/location/${this.state.location}`)
    .then((res) => {
      if(res.data.success === true) {
        this.setState({topTenCharts: res.data.data });
      }
    })
    await httpClient.get(`/api/dashboard/grid/${this.props.trackId}/location/${this.state.location}`)
    .then((res) => {
      if(res.data.success === true) {
        let data = res.data.data;
        data = data.sort((a, b) => a.downloads < b.downloads ? 1 : a.downloads > b.downloads ? -1 : 0)
        this.setState({allGrid: data });
      }
    })

    setTimeout(() => {
      this.initMapChart();
      this.initBarChart();
      this.setState({loading: false});
    }, 200);

    // http://f050518b.ngrok.io/api/dashboard/markers/143756/location/US
    // http://f050518b.ngrok.io/api/dashboard/chart/143756/location/US
    // http://f050518b.ngrok.io/api/dashboard/grid/143756/location/US

  }
  

  location = null;
  componentWillReceiveProps(nextProps) {
    if((!this.location && nextProps.selectedCountry) || (this.location !== nextProps.selectedCountry)) {
      this.location = nextProps.selectedCountry;
      this.setState({ location: nextProps.selectedCountry });
      setTimeout(() => {
        this.getData();
        document.getElementById('country-charts').scrollIntoView();
      }, 100);
    }
  }

  createSeries = async (heatfield) => {
    let series = this.state.chart.series.push(new am4maps.MapImageSeries());
    series.dataFields.value = heatfield;

    let template = series.mapImages.template;
    template.verticalCenter = "middle";
    template.horizontalCenter = "middle";
    template.propertyFields.latitude = "lat";
    template.propertyFields.longitude = "long";
    template.tooltipText = "{name}:\n[bold]{value} downloads[/]";
    template.fontSize = 8;

    let circle = template.createChild(am4core.Circle);
    circle.radius = 10;
    circle.fillOpacity = 0.7;
    circle.fill = am4core.color('red');
    circle.verticalCenter = "middle";
    circle.horizontalCenter = "middle";
    circle.nonScaling = true;

    let label = template.createChild(am4core.Label);
    label.text = "{value}";
    label.fill = am4core.color("#fff");
    label.verticalCenter = "middle";
    label.horizontalCenter = "middle";
    label.nonScaling = true;
    label.fontSize = 12;
    
    
    series.mapImages.template.events.on("hit", function(ev) {
      var data = ev.target.dataItem.dataContext;
      this.setState({
        selectedCountry: data.country_code
      })
    }.bind(this));
    // let heat = series.heatRules.push({
    //   target: circle,
    //   property: "radius",
    //   min: 10,
    //   max: 30
    // });

    circle.adapter.add("radius", function(radius, target) {
      if (target.dataItem) {
        if (target.dataItem.value >= 100) {
          return 30
        }
        else if (target.dataItem.value >= 10) {
          return 18;
        }
        else {
          return 10;
        }
      }
      return radius;
    });

    circle.adapter.add("fill", function(fill, target) {
      if (target.dataItem) {
        if (target.dataItem.value >= 100) {
          return am4core.color("#b44233")
        }
        else if (target.dataItem.value >= 10) {
          return am4core.color("#f1c027")
        }
        else {
          return am4core.color("#009de0");
        }
      }
      return fill;
    });

    return series;
  }

  async setupStores(data) {
    // Set current series
    let regionalSeries = {
      markerData: [],
      series: await this.createSeries("stores")
    };

    // Process data
    am4core.array.each(data.markers, function(store) {

      // Get store data
      let country = {
        state: store.region_code,
        long: am4core.type.toNumber(store.longitude),
        lat: am4core.type.toNumber(store.latitude),
        location: store.region_code,
        city: null,
        count: am4core.type.toNumber(store.count),
        country_code: store.country_code
      };

      if(country.lat && country.long) {
        // Process individual store
        regionalSeries.markerData.push({
          name: country.location,
          count: country.count,
          stores: country.count,
          lat: country.lat,
          long: country.long,
          state: country.state,
          country_code: country.country_code
        });
      }

    });

    regionalSeries.series.data = regionalSeries.markerData;

    this.setState({
      regionalSeries
    });
  }

  initMapChart = async () => {
    if (this.state.chart) {
      this.state.chart.dispose();
    }

    if(this.countryMaps[ this.state.location ] === undefined) {
      return;
    }

    // calculate which map to be used
    let currentMap = this.countryMaps[ this.state.location ][ 0 ];
    
    // Create map instance
    let chart = am4core.create("countryMapChart", am4maps.MapChart);
    chart.maxZoomLevel = 3;

    // Set map definition
    chart.geodataSource.url = "https://www.amcharts.com/lib/4/geodata/json/" + currentMap + ".json";
    chart.geodataSource.events.on("parseended", function(ev) {
      let data = [];
      for(var i = 0; i < ev.target.data.features.length; i++) {
        data.push({
          id: ev.target.data.features[i].id,
          value: Math.round( Math.random() * 10000 )
        })
      }
      polygonSeries.data = data;
    })

    // Set projection
    chart.projection = new am4maps.projections.Mercator();

    // Create map polygon series
    let polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

    // Make map load polygon data (state shapes and names) from GeoJSON
    polygonSeries.useGeodata = true;

    chart.events.on("ready", () => {
      this.setupStores(this.state.markers)
    });

    this.setState({
      chart: chart
    })
  }

  initBarChart = async () => {
    const chart = {
      data: {
        labels: [],
        datasets: [
          {
            label: 'Downloads',
            backgroundColor: 'rgba(208,14,14,1)',
            borderColor: 'rgba(208,14,14,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(208,14,14,1)',
            hoverBorderColor: 'rgba(208,14,14,1)',
            data: []
          }
        ]
      },
      options: {
        legend: {
            labels: {
              fontColor: "white",
              fontSize: 18
            }
        },
        scales: {
            yAxes: [{
              ticks: {
                fontColor: "white"
              }
            }],
            xAxes: [{
              display: false
            }]
        },
        plugins: {
          datalabels: {
            color: '#fff'
          }
        },
        responsive: true
      },
      legend: {
        "display": false
      }
    }


    if(this.state.topTenCharts) {
      const topTenCharts = this.state.topTenCharts;
      chart.data.labels = topTenCharts.map(l => l.name);
      chart.data.datasets[0].data = topTenCharts.map(l => l.count);
    }

    this.state.barChart = chart;
  }

  onCitySelect = async (e) => {
    const city = e.target.getAttribute('data-city');
    if(city) {
      this.props.onCitySelect({city});
    }
  }

  render() {
    const barChart = () => {
      if(!this.state.barChart) {
        return null;
      }
      return <div style={{border: '2px solid #dee2e6'}}><HorizontalBar data={this.state.barChart.data} height={200} options={this.state.barChart.options} legend={this.state.barChart.legend} /></div>;
    }
    
    const items = () => {
      if(!this.state.allGrid) {
        return null;
      }

      let table = []

      this.state.allGrid.filter(g => g.listens > 0 || g.downloads > 0).forEach((item, i) => {
        table.push(
          <tr key={i}>
            <td>{item.name}</td>
            <td>{item.listens}</td>
            <td>{item.downloads}</td>
            <td><button className="btn btn-light btn-sm" data-city="NY" onClick={this.onCitySelect}>View</button></td>
          </tr>
        )
      });

      return table
    }

    return (
      <div>
      { this.state.location ?
        <div className="container text-center text-white pb-5" id="country-charts">
          <div className="text-center"><h4 className="title-outline">World - {this.state.location.toUpperCase()}</h4></div>
          <div className="row justify-content-md-center">
            <div className="col-md-10">
              <div className="mb-4">

                <div className={this.state.loading === 1 ? 'hidden' : ''}>
                  <p className="text-center my-5 px-4" style={{ fontSize: '13.5px',fontWeight: '500' }}>
                    On the line graph below you can see the top 10 countries your track "{this.state.markers && this.state.markers.artist? `${this.state.markers.artist} - ${this.state.markers.name}`: ''}" has been downloaded.
                  </p>
                  <div className="row mb-3">
                    <div className="col-md-6">
                      {barChart()}
                    </div>
                    <div className="col-md-6">
                      <div id="countryMapChart" style={{height: '300px', display: 'block', width: '100%', backgroundColor: '#fff'}}></div>
                    </div>
                  </div>
                  <p className="text-center mt-5 mb-5" style={{ fontSize: '13.5px',fontWeight: '500' }}>Summary Table Of Downloads By Country</p>
    
                  <div className="table-responsive" style={{maxHeight: '360px', borderBottom: '1px solid #dee2e6'}}>
                    <table className="table table-sm table-bordered table-striped sticky-header frp-table" style={{marginBottom: '-1px'}}>
                      <thead>
                        <tr className="bg-dark text-white">
                          <th style={{boxShadow: '-5px -5px 3px #0d1c2c'}}>State</th>
                          <th>Listened</th>
                          <th>Downloaded</th>
                          <th style={{boxShadow: '5px -1px 0px #0d1c2c'}}>Actions</th>
                        </tr>
                      </thead>
                      <tbody className="text-white">
                        {items()}
                      </tbody>
                    </table>
                  </div>
                </div>

                <p className={this.state.loading === 1 ? 'text-center' : 'hidden'}>
                  Please wait...
                </p>

              </div>
            </div>
          </div>
        </div>
      : ''
      }
      </div>
    )

  }
}

export default CountryDownloadCount;

import React from 'react';

import SearchTrackForm from './SearchTrackForm';
import Feedback from './Feedback';
import GlobalDownloadCount from './GlobalDownloadCount';
import CountryDownloadCount from './CountryDownloadCount';
import CityFeedbacks from './CityFeedbacks';
import httpClient from '../../../utils/providers/httpClient';

class DashboardPage extends React.Component {
  state = {
    trackId: 143756,
    trackName: null,
    counts: null,
    downloadMapMarkers: null,
    selectedCountry: null,
    selectedCity: null
  }

  getLabelTrack = () => {
    // axios.get(`https://www.franchiserecordpool.com/trending/global/markers/format/json/trackid/${this.state.trackId}`)
    // .then((res) => {
    //   this.setState({trackName: res.data.queryTitle });
    // })

    // axios.get(`https://www.franchiserecordpool.com/trending/global/statstableview/dataset/3/type/state-track/id/${this.state.trackId}`)
    // .then((res) => {
    //   this.setState({counts: res.data.data[0] });
    // })

    httpClient.get(`api/dashboard/trackdata/${this.state.trackId}`)
    .then((res) => {
      if(res.data.success === true) {
        this.setState({counts: res.data.data });
      }
    })

    // /api/dashboard/trackdata/:trackid
  }

  getDownloadMapMarkers = () => {
    // Get downloads by country
    httpClient.get(`/api/dashboard/downloadscountryregion/${this.state.trackId}`)
    .then((res) => {
      if(res.data.success === true) {
        this.setState({
          downloadMapMarkers: res.data.data,
          trackName: res.data.data && res.data.data.artist ? `${res.data.data.artist} ${res.data.data.name}` : ''
        });
      }
    })
  }

  onTrackChange = (track) => {
    this.setState({
      trackId: track.id,
      selectedCountry: null,
      selectedCity: null
    })
    setTimeout(() => {
      this.initTrack();
    }, 500);
  }

  componentWillMount() {
    this.initTrack();
  }

  async initTrack() {
    this.getLabelTrack();
    this.getDownloadMapMarkers();
  }

  onCountrySelect = (data) => {
    this.setState({
      selectedCountry: data.country_code,
      selectedCity: null
    });
  }

  onCitySelect = (data) => {
    this.setState({
      selectedCity: data.city
    });
  }

  render() {
    return (
      <div className="wrapper label-dashboard">
        <SearchTrackForm trackName={this.state.trackName} counts={this.state.counts} onTrackChange={this.onTrackChange} />
        <Feedback trackId={this.state.trackId} counts={this.state.counts} onCountrySelect={this.onCountrySelect} />
        <GlobalDownloadCount trackId={this.state.trackId} downloadMapMarkers={this.state.downloadMapMarkers} onCountrySelect={this.onCountrySelect} />
        <CountryDownloadCount trackId={this.state.trackId} selectedCountry={this.state.selectedCountry} onCitySelect={this.onCitySelect} />
        <CityFeedbacks trackId={this.state.trackId} selectedCountry={this.state.selectedCountry} selectedCity={this.state.selectedCity}  />
      </div>
    )
  }
}

export default DashboardPage;

// HomePage.propTypes = {
//   loading: PropTypes.bool,
//   error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
//   repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
//   onSubmitForm: PropTypes.func,
//   username: PropTypes.string,
//   onChangeUsername: PropTypes.func,
// };

// const mapStateToProps = createStructuredSelector({
//   repos: makeSelectRepos(),
//   username: makeSelectUsername(),
//   loading: makeSelectLoading(),
//   error: makeSelectError(),
// });

// export function mapDispatchToProps(dispatch) {
//   return {
//     onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
//     onSubmitForm: evt => {
//       if (evt !== undefined && evt.preventDefault) evt.preventDefault();
//       dispatch(loadRepos());
//     },
//   };
// }

// const withConnect = connect(
//   mapStateToProps,
//   mapDispatchToProps,
// );

// export default compose(
//   withConnect,
//   memo,
// )(HomePage);

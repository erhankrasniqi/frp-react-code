import React, { Component, Fragment } from 'react'

import StarIcon from '../../../images/star.svg';
import StarsIcon from '../../../images/stars.svg';
import HeadphonesIcon from '../../../images/headphones.svg';
import MusicIcon from '../../../images/musical-note.svg';
import TopDownloads from './Feedbacks/TopDownloads';
import DownloadListen from './Feedbacks/DownloadListen';

import FeedbackFromDJs from './Feedbacks/FeedbackFromDJs';
import TrackRatings from './Feedbacks/TrackRatings';
import httpClient from '../../../utils/providers/httpClient';

export default class Feedback extends Component {


  constructor(props) {
    super(props);
    this.state = {
      activeTab: 1,
      feedbackByDjs: null,
      trackRatings: null,
      downloadsByCountry: null
    }

    this.setActiveTab = this.setActiveTab.bind(this);
  }
  
  getTopDownloads = (trackId) => {
    // Get downloads by country
    httpClient.get(`/api/dashboard/topdownloads/${trackId ? trackId : this.props.trackId}`)
    .then((res) => {
      if(res.data.success === true) {
        this.setState({downloadsByCountry: res.data.data });
      }
    });
  }

  getFeedbackByDJs = () => {
    httpClient.get(`/api/dashboard/feedbackdj/${this.props.trackId}`)
    .then((res) => {
      if(res.data.success === true) {
        this.setState({feedbackByDjs: res.data.data });
      }
    })
  }

  getTrackRatings = () => {
    httpClient.get(`/api/dashboard/rating/${this.props.trackId}`)
    .then((res) => {
      if(res.data.success === true) {
        this.setState({trackRatings: res.data.data });
      }
    })

  }

  trackId = null;
  componentWillReceiveProps(nextProps) {
      if (!nextProps.trackId || (this.trackId && this.trackId === nextProps.trackId)) {
        return;
      }
      this.trackId = nextProps.trackId;

      this.setState({
        feedbackByDjs: null,
        trackRatings: null
      })
  
      this.getTopDownloads(nextProps.trackId);
      this.setActiveTab(this.state.activeTab);
  }

  setActiveTab(tab) {
    this.setState({
      activeTab: tab
    });

    if(tab === 3 && !this.state.feedbackByDjs) {
      this.getFeedbackByDJs();
    }

    if(tab === 4 && !this.state.trackRatings) {
      this.getTrackRatings();
    }
  }

  render() {

    return (
      <section id="feedback" className={this.props.className ? this.props.className : ''}>
        <div className="container">
          <div className="row justify-content-md-center">
            <div className="col-md-10">
              <h4 className="text-center">Feedback</h4>
              <ul className="feedback-tabs">
                <li className={this.state.activeTab === 1 ? 'active' : null}>
                  <a onClick={(e) => this.setActiveTab(1)}>
                    <figure style={{margin: '10px 0 0 0'}}>
                      <img src={StarIcon} width="22" height="22" alt="Top downloads" /> 
                    </figure>
                    Top Downloads
                  </a>
                </li>
                <li className={this.state.activeTab === 2 ? 'active' : null}>
                  <a onClick={(e) => this.setActiveTab(2)}>
                    <figure style={{margin: '10px 0 0 0'}}>
                      <img src={HeadphonesIcon} width="22" height="22" alt="Download vs Listen" /> 
                    </figure>
                    Download vs Listen
                  </a>
                </li>
                <li className={this.state.activeTab === 3 ? 'active' : null}>
                  <a onClick={(e) => this.setActiveTab(3)}>
                    <figure style={{margin: '10px 0 0 0'}}>
                      <img src={StarsIcon} width="32" height="22" alt="Feedback from Djs" /> 
                    </figure>
                    Feedback from Djs
                  </a>
                </li>
                <li className={this.state.activeTab === 4 ? 'active' : null}>
                  <a onClick={(e) => this.setActiveTab(4)}>
                    <figure style={{margin: '10px 0 0 0'}}>
                      <img src={MusicIcon} width="22" height="22" alt="Track Ratings" /> 
                    </figure>
                    Track Ratings
                  </a>
                </li>
              </ul>

              <div className={this.state.activeTab === 1 ? 'feedback-card' : 'hidden'}>
                <TopDownloads downloadsByCountry={this.state.downloadsByCountry} onCountrySelect={this.props.onCountrySelect} /> 
              </div>
              <div className={this.state.activeTab === 2 ? 'feedback-card' : 'hidden'}>
                <DownloadListen download={this.props.counts ? this.props.counts.downloads : 0} listen={this.props.counts ? this.props.counts.listens : 0} /> 
              </div>
              <div className={this.state.activeTab === 3 ? 'feedback-card' : 'hidden'}>
                <FeedbackFromDJs data={this.state.feedbackByDjs} /> 
              </div>
              <div className={this.state.activeTab === 4 ? 'feedback-card' : 'hidden'}>
                <TrackRatings data={this.state.trackRatings} /> 
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

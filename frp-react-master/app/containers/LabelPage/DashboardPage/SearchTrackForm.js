import React, { Component } from 'react';
import httpClient from '../../../utils/providers/httpClient';

class SearchTrackForm extends Component {

    constructor(props) {
      super(props);

      this.state = {
        query: null,
        typing: false,
        typingTimeout: 0,
        searchResult: null
      }

      this.onSearchChange = this.onSearchChange.bind(this);
    }
    onSearch = (e) => {
      e.preventDefault();
      const query = e.target.elements.query.value;
      if(!query) {
        this.setState({
          searchResult: null
        })
      } else {

        httpClient.post(`/api/dashboard/search`, {
          text: query
        })
        .then((res) => {
          if(res.data.success === true) {
            this.setState({
              searchResult: res.data.data.map(d => ({label: `${d.artiste} - ${d.song_name}`, id: d.id}))
            })
          }
        })
      }
    }

    search(query) {
      if(!query) {
        this.setState({
          searchResult: null
        })
      } else {
        httpClient.post(`/api/dashboard/search`, {
          text: query
        })
        .then((res) => {
          if(res.data.success === true) {
            this.setState({
              searchResult: res.data.data.map(d => ({label: `${d.artiste} - ${d.song_name}`, id: d.id}))
            })
          }
        })
      }
    }

    onSearchChange = (event) => {
      const self = this;
  
      if (self.state.typingTimeout) {
        clearTimeout(self.state.typingTimeout);
      }
  
      self.setState({
        query: event.target.value,
        typing: false,
        typingTimeout: setTimeout(() => {
            self.search(self.state.query);
          }, 500)
      });
    }

    onTrackSelect = (track) => {
      this.setState({
        searchResult: null
      })
      this.props.onTrackChange(track);

    }

    render() {
      const autoCompleteResult = () => {
        if(!this.state.searchResult) {
          return null;
        }
        
        let items = [];

        this.state.searchResult.forEach((item) => {
          let handleClick = (e) => {
            e.preventDefault();
            this.onTrackSelect(item);
          };
          items.push(
            <a key={item.id} href="#" onClick={handleClick} className="list-group-item list-group-item-action">
              {item.label}
            </a>
          )
        });

        return (
          <div className="list-group" id="search-result" role="tablist">
            {items}
          </div>
        )
      };

      return (
        <div className="search-track-form">
          <div className="container text-center">
            <div className="row justify-content-md-center">
              <div className="col-md-6">
                <h3 style={{marginBottom: '28px'}}>Search Track</h3>
                <form onSubmit={this.onSearch}>
                  <div className="form-group">
                    <input type="search" placeholder="Track Name" name="query" className="form-control form-control-lg" id="search-input" onChange={this.onSearchChange} />
                    {autoCompleteResult()}
                  </div>
                </form>
                <h4 className="track-name mt-4 mb-4">{this.props.trackName}</h4>
                <div className="counts">
                  <div className="count-block align-middle">
                    <div className="count-block-digits">{this.props.counts ? this.props.counts.downloads : null }</div>
                    <div className="count-block-label">Downloads</div>
                  </div>
                  <div className="count-block align-middle">
                    <div className="count-block-digits">{this.props.counts ? this.props.counts.listens : null}</div>
                    <div className="count-block-label">Listen</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
}

export default SearchTrackForm;
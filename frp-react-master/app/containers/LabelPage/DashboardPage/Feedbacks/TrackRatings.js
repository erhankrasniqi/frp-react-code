import React from 'react'
import {Bar} from 'react-chartjs-2';

function TrackRatings(props) {
  const data = {
    labels: [],
    datasets: [
      {
        label: 'Wack',
        backgroundColor: '#fe2b6c',
        data: []
      },
      {
        label: 'OK',
        backgroundColor: '#487eb6',
        data: []
      },
      {
        label: 'Good',
        backgroundColor: '#a7ce31',
        data: []
      },
      {
        label: 'Great',
        backgroundColor: '#f8cc01',
        data: []
      },
      {
        label: 'Classic',
        backgroundColor: '#ff8c01',
        data: []
      }
    ]
  };

  const options = {
    plugins: {
      datalabels: {
        color: '#fff'
      }
    }
  }

  const legend = {
    "display": true,
    "position": "right"
  };

  if(props && props.data) {
    data.labels = props.data.map(l => l.label);
    data.datasets.forEach((dataset, index) => {
      data.datasets[index].data = props.data.map(l => l[dataset.label])
    });
  }

  if(props && props.data) {
    return (
      <div>
        <Bar data={data} width={890} height={290} options={options} legend={legend} />
        <p className="pt-2 mb-0">This chart shows the number of DJs who think the Instrumental (beat) is wack, ok, good, great or a classic or the vocal (acapella) is wack, ok, good, great or a classic. The last section (DJ) is what the DJ thinks of the overall track.</p>
      </div>
    )
  } else {
    return <p className="text-center">Please wait...</p>
  }
}

export default TrackRatings;
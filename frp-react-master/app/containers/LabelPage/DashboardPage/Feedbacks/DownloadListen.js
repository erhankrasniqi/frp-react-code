import React from 'react'
import { Pie } from 'react-chartjs-2'
import ChartDataLabels from 'chartjs-plugin-datalabels'

function DownloadListen(props) {

    const data = {
      labels: [
        'Download',
        'Listen'
      ],
      datasets: [{
        data: [props.download, props.listen],
        backgroundColor: [
          '#fe2b6c',
          '#487eb6'
        ],
        hoverBackgroundColor: [
          '#fe2b6c',
          '#487eb6' 
        ]
      }]
    };

    const options = {
      plugins: {
          datalabels: {
              formatter: (value, ctx) => {
                  let sum = 0;
                  let dataArr = ctx.chart.data.datasets[0].data;
                  dataArr.map(data => {
                      sum += data;
                  });
                  let percentage = (value*100 / sum).toFixed(2)+"%";
                  return percentage;
              },
              color: '#fff',
          }
      }
    }

    const legend = {
      "display": true,
      "position": "right"
    };

    return (
      <div>
        <Pie data={data} options={options} legend={legend} width={890} height={285} />
        <p className="pt-2 mb-0">This Pie Chart shows you the difference between the download and listen count. A larger listen count tells you its possible the track is not buzzing enough to encourage download.  A larger download count tells you its possible the track has major potential.</p>
      </div>
    )
}

export default DownloadListen;

import React from 'react'
import {Bar} from 'react-chartjs-2';

function FeedbackFromDJs(props) {
  const data = {
    labels: [],
    datasets: [
      {
        label: 'No',
        backgroundColor: 'rgba(253,43,108,0.8)',
        borderColor: 'rgba(253,43,108,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(253,43,108,0.9)',
        hoverBorderColor: 'rgba(253,43,108,1)',
        data: []
      },
      {
        label: 'Yes',
        backgroundColor: 'rgba(72,126,182,0.8)',
        borderColor: 'rgba(72,126,182,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(72,126,182,0.9)',
        hoverBorderColor: 'rgba(72,126,182,1)',
        data: []
      }
    ]
  };

  const options = {
    plugins: {
      datalabels: {
        color: '#fff'
      }
    }
  }

  const legend = {
    "display": true,
    "position": "right"
  };

  if(props && props.data) {
    data.labels = props.data.map(l => l.label);
    data.datasets[0].data = props.data.map(l => l.no);
    data.datasets[1].data = props.data.map(l => l.yes);
  }

  if(props && props.data) {
    return (
      <div>
        <Bar data={data} width={890} height={315} options={options} legend={legend} />
        <p className="pt-2 mb-0">This chart shows you how many members said yes or no to our default questions.Blue is yes and red is no.</p>
      </div>
    )
  } else {
    return <p className="text-center">Please wait...</p>
  }
}

export default FeedbackFromDJs;
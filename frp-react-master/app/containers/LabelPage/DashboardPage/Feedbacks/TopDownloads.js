import React, { Component } from 'react'

// Import Chart libraries
import * as am4core from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";

am4core.useTheme(am4themes_animated);

export default class TopDownloads extends Component {


  constructor(props) {
    super(props);
    this.state = {
      activeTab: 1,
      topDownloads: null,
      filteredTopDownloads: null
    }

    this.setActiveTab = this.setActiveTab.bind(this);
  }

  setActiveTab(tab) {
    this.setState({
      activeTab: tab
    });
  }
  
  componentDidMount() {
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  downloadsByCountry = null;
  componentWillReceiveProps(nextProps) {
    if (!nextProps.downloadsByCountry || (this.downloadsByCountry && this.downloadsByCountry === nextProps.downloadsByCountry)) {
      return;
    }

    this.downloadsByCountry = nextProps.trackId;
    
    const topDownloads = nextProps.downloadsByCountry.sort((a, b) => b.count - a.count);
    this.setState({ topDownloads: topDownloads });
    if(!this.state.filteredTopDownloads || this.state.filteredTopDownloads.length !== this.state.topDownloads.length) {
      this.setState({ filteredTopDownloads: topDownloads });
    }
    this.initChart();
  }

  onCountrySearch = (e) => {
    if(!this.state.topDownloads) {
      return;
    }
    if(!e.target.value) {
      this.setState({ filteredTopDownloads: this.state.topDownloads });
      return;
    }
    try {
      this.setState({
        filteredTopDownloads: this.state.topDownloads.filter(x => x.country_code.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1).sort((a, b) => b.count - a.count)
      })
    } catch(e) {

    }
  }

  onCountryClick = (data) => {
    const topDownloads = this.state.topDownloads;
    const selectedCountry = topDownloads.filter(d => d.country_code === data.name);
    if(selectedCountry && selectedCountry.length > 0) {
      this.props.onCountrySelect({country_code: data.id, country_name: data.name});
    }
  }

  async initChart() {
    // Create map instance
    let chart = am4core.create("chartdiv", am4maps.MapChart);
    chart.maxZoomLevel = 3;

    // const data = am4geodata_worldLow.map((obj, index) => ({
    //   ...obj,
    //   features: obj.features.map((ftr, i) => ({...ftr, value: Math.round( Math.random() * 10000 )}))
    // }))
    // Set map definition
    chart.geodata = am4geodata_worldLow;
    

    // Set projection
    chart.projection = new am4maps.projections.Miller();

    // Series for World map
    let worldSeries = chart.series.push(new am4maps.MapPolygonSeries());
    worldSeries.exclude = ["AQ"];
    //Set min/max fill color for each area
    worldSeries.heatRules.push({
      property: "fill",
      target: worldSeries.mapPolygons.template,
      min: chart.colors.getIndex(1).brighten(1),
      max: chart.colors.getIndex(1).brighten(-0.3)
    });
    worldSeries.useGeodata = true;


    worldSeries.mapPolygons.template.events.on("hit", function(ev) {
      var data = ev.target.dataItem.dataContext;
      this.onCountryClick(data);
    }.bind(this));

    const getValue = (country_code) => {
      const items = this.state.topDownloads.filter(d => d.country_code === country_code);
      return items.length > 0 ? items[0].count : null;
    }
    
    var updatedData = false;
    // Update heat legend value labels
    worldSeries.events.on("beforedatavalidated", function(ev) {
      if(!updatedData) {
        worldSeries.data = worldSeries.data.map((obj, index) => ({
          ...obj,
          value: getValue(obj.name)
        }))
        updatedData = true;
      }
    });

    // Set up heat legend
    let heatLegend = chart.createChild(am4maps.HeatLegend);
    heatLegend.id = "heatLegend";
    heatLegend.series = worldSeries;
    heatLegend.align = "right";
    heatLegend.valign = "bottom";
    heatLegend.width = am4core.percent(25);
    // heatLegend.marginRight = am4core.percent(4);
    heatLegend.background.fill = am4core.color("#000");
    heatLegend.background.fillOpacity = 0.05;
    heatLegend.padding(5, 5, 0, 5);
    heatLegend.fontSize = '12px';

    // Set up custom heat map legend labels using axis ranges
    let minRange = heatLegend.valueAxis.axisRanges.create();
    minRange.label.horizontalCenter = "left";

    let maxRange = heatLegend.valueAxis.axisRanges.create();
    maxRange.label.horizontalCenter = "right";

    // Blank out internal heat legend value axis labels
    heatLegend.valueAxis.renderer.labels.template.adapter.add("text", function(labelText) {
      return "";
    });

    worldSeries.events.on("datavalidated", function(ev) {
      let heatLegend = ev.target.map.getKey("heatLegend");

      let min = heatLegend.series.dataItem.values.value.low;
      let minRange = heatLegend.valueAxis.axisRanges.getIndex(0);
      minRange.value = min;
      minRange.label.text = "" + heatLegend.numberFormatter.format(min);

      let max = heatLegend.series.dataItem.values.value.high;
      let maxRange = heatLegend.valueAxis.axisRanges.getIndex(1);
      maxRange.value = max;
      maxRange.label.text = "" + heatLegend.numberFormatter.format(max);

    });
    // Configure series tooltip
    var polygonTemplate = worldSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}: {value}";
    polygonTemplate.nonScalingStroke = true;
    polygonTemplate.fontSize = '12px';
    polygonTemplate.strokeWidth = 0.5;

    // Create hover state and set alternative fill color
    var hs = polygonTemplate.states.create("hover");
    hs.properties.fill = chart.colors.getIndex(1).brighten(-0.5);

  }

  render() {
    const items = () => {
      if(!this.state.topDownloads) {
        return null;
      }

      let table = []
  
      for (let i = 0; i < this.state.filteredTopDownloads.length; i++) {
        const item = this.state.filteredTopDownloads[i];
        table.push(
          <tr key={i}>
            <td>{item.country_code}</td>
            <td>{item.count}</td>
          </tr>
        )
      }
      return table
    }

    return (
      <div style={{ width: "100%", height: "100%" }}>
        <ul className="feedback-switch">
          <li className={this.state.activeTab === 1 ? 'active' : ''}><a onClick={(e) => this.setActiveTab(1)}>Map View</a></li>
          <li className={this.state.activeTab === 2 ? 'active' : ''}><a onClick={(e) => this.setActiveTab(2)}>Text View</a></li>
        </ul>
        <div className={this.state.activeTab === 1 ? '' : 'hidden'}>
          <div id="chartdiv" style={{ width: "100%", height: "335px" }}></div>
          <p className="mt-2 mb-0">This section give a birds eye view of the parts of the world where your tracks has been downloaded.</p>
        </div>
        <div className={this.state.activeTab === 2 ? '' : 'hidden'} >
          <form className="inline-form">
            <div className="row">
              <div className="col-3 form-group">
                <input type="text" name="searchQuery" className="form-control from-control-sm" onChange={this.onCountrySearch} placeholder="Search" />
              </div>
            </div>
          </form>
          <div className="table-responsive" style={{maxHeight: '320px'}}>
            <table className="table table-sm table-bordered table-striped sticky-header">
              <thead>
                <tr className="bg-primary text-white">
                  <th>Country</th>
                  <th>Total Count</th>
                </tr>
              </thead>
              <tbody>
                {items()}
              </tbody>
            </table>
          </div>
        </div>
      </div> 
    )
  }
}

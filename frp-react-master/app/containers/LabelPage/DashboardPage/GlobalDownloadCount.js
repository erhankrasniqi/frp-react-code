import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Import Chart libraries
import * as am4core from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";

am4core.useTheme(am4themes_animated);

export default class GlobalDownloadCount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      topDownloads: null,
      chart: null,
      selectedCountry: null
    }

    this.createSeries.bind(this);
    // this.selectCountry.bind(this);
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  trackId = null;
  componentWillReceiveProps(nextProps) {
    if(!this.trackId || (nextProp.trackId && nextProps.downloadMapMarkers && this.trackId !== nextProps.trackId)) {
      this.setState({ topDownloads: nextProps.downloadMapMarkers });
      setTimeout(() => {
        this.initChart();
      }, 300);
    }
  }

  createSeries = async (heatfield) => {
    let series = this.state.chart.series.push(new am4maps.MapImageSeries());
    series.dataFields.value = heatfield;

    let template = series.mapImages.template;
    template.verticalCenter = "middle";
    template.horizontalCenter = "middle";
    template.propertyFields.latitude = "lat";
    template.propertyFields.longitude = "long";
    template.tooltipText = "{name}:\n[bold]{value} downloads[/]";
    template.fontSize = 8;

    let circle = template.createChild(am4core.Circle);
    circle.radius = 10;
    circle.fillOpacity = 0.7;
    circle.fill = am4core.color('red');
    circle.verticalCenter = "middle";
    circle.horizontalCenter = "middle";
    circle.nonScaling = true;

    let label = template.createChild(am4core.Label);
    label.text = "{value}";
    label.fill = am4core.color("#fff");
    label.verticalCenter = "middle";
    label.horizontalCenter = "middle";
    label.nonScaling = true;
    label.fontSize = 12;
    
    
    series.mapImages.template.events.on("hit", function(ev) {
      var data = ev.target.dataItem.dataContext;
      this.onCountrySelect(data);
    }.bind(this));
    // let heat = series.heatRules.push({
    //   target: circle,
    //   property: "radius",
    //   min: 10,
    //   max: 30
    // });

    circle.adapter.add("radius", function(radius, target) {
      if (target.dataItem) {
        if (target.dataItem.value >= 100) {
          return 30
        }
        else if (target.dataItem.value >= 10) {
          return 18;
        }
        else {
          return 10;
        }
      }
      return radius;
    });

    circle.adapter.add("fill", function(fill, target) {
      if (target.dataItem) {
        if (target.dataItem.value >= 100) {
          return am4core.color("#b44233")
        }
        else if (target.dataItem.value >= 10) {
          return am4core.color("#f1c027")
        }
        else {
          return am4core.color("#009de0");
        }
      }
      return fill;
    });

    return series;
  }

  async setupStores(data) {
    if(!data || !data.markers) {
      return;
    }

    // Set current series
    let regionalSeries = {
      markerData: [],
      series: await this.createSeries("stores")
    };

    // Process data
    am4core.array.each(data.markers, function(store) {

      // Get store data
      let country = {
        state: store.country_name,
        long: am4core.type.toNumber(store.longitude),
        lat: am4core.type.toNumber(store.latitude),
        location: store.country_name,
        city: null,
        count: am4core.type.toNumber(store.count),
        country_code: store.country_code
      };

      // Process individual store
      regionalSeries.markerData.push({
        name: country.location,
        count: country.count,
        stores: country.count,
        lat: country.lat,
        long: country.long,
        state: country.state,
        country_code: country.country_code
      });

    });

    regionalSeries.series.data = regionalSeries.markerData;

    this.setState({
      regionalSeries
    });
  }

  async initChart() {
    if (this.state.chart) {
      this.state.chart.dispose();
    }

    // Create map instance
    let chart = am4core.create("globalDownloadsMap", am4maps.MapChart);
    chart.maxZoomLevel = 3;

    // Set map definition
    chart.geodata = am4geodata_worldLow;
    chart.events.on("ready", () => {
      this.setupStores(this.state.topDownloads)
    });
    

    // Series for World map
    let worldSeries = chart.series.push(new am4maps.MapPolygonSeries());
    worldSeries.exclude = ["AQ"];
    worldSeries.useGeodata = true;


    this.setState({
      chart: chart
    })
  }

  onCountrySelect = (data) => {
    console.log(data);
    this.props.onCountrySelect(data);
  }

  render() {
    const circleStyle = {
      display: 'inline-block',
      height: '10px',
      width: '10px',
      borderRadius: '50%',
      marginRight: '5px'
    }
    const redCircle = {
      ...circleStyle,
      backgroundColor: '#b44233'
    }
    const yellowCircle = {
      ...circleStyle,
      backgroundColor: '#f1c027'
    }
    const blueCircle = {
      ...circleStyle,
      backgroundColor: '#009de0'
    }

    return (
      <div>
        <section id="global-download-count" className="world-map-bg">
          <div className="container text-center text-white pt-4" style={{paddingBottom: '100px'}}>
            <h4 className="text-center">Global Download Count</h4>
            <h6 className="mt-3 mb-5" style={{fontSize: '13.5px'}}>Map view of download count via countries with push pin effect with ability to drill down to city level per country</h6>

            <div className="row justify-content-md-center">
              <div className="col-md-10">
                <div className="feedback-card mb-4">
                  <div id="globalDownloadsMap" style={{ width: "100%", height: "335px" }}></div>
                  <div className="row justify-content-md-center mt-3" style={{fontSize: '14px'}}>
                    <div className="col-sm-4"><div style={blueCircle}></div> {'Number of downloads < 10'}</div>
                    <div className="col-sm-4"><div style={yellowCircle}></div> {'Number of downloads < 100'}</div>
                    <div className="col-sm-4"><div style={redCircle}></div> {'Number of downloads > 99'}</div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </section>
      </div>
    )
  }
}



GlobalDownloadCount.propTypes = {
  trackId: PropTypes.number,
  downloadMapMarkers: PropTypes.object,
  onCountrySelect: PropTypes.func
};
import React, { Component } from 'react'
import BusinessOwnership from './CityFeedbacks/BusinessOwnership';
import AgeBreakDown from './CityFeedbacks/AgeBreakDown';
import SexBreakDown from './CityFeedbacks/SexBreakDown';
import EthnicBreakDown from './CityFeedbacks/EthnicBreakDown';

import StarIcon from '../../../images/star.svg';
import StarsIcon from '../../../images/stars.svg';
import MusicIcon from '../../../images/musical-note.svg';
import BusinessOwnershipIcon from '../../../images/icon-business-ownership.png'
import AgeBreakDownIcon from '../../../images/icon-age-break-down.png'
import SexBreakDownIcon from '../../../images/icon-sex-break-down.png'
import EthnicBreakDownIcon from '../../../images/icon-ethnic-break-down.png'
import httpClient from '../../../utils/providers/httpClient';

export default class CityFeedbacks extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activeTab: 1,
      viewMode: 'mapView',
      trackId: null,
      selectedCountry: null,
      selectedCity: null,

      // businessOwnership: null,
      // ageBreakDown: null,
      // sexBreakDown: null,
      // ethnicBreakDown: null,
      // feedbackFromDJs: null,
      // trackRatings: null
      businessOwnership: [
        ["Asian", 10.1],
        ["Black", 10.4],
        ["Hawaii", 0.1],
        ["Hispanic", 9.9],
        ["Indian", 0.7],
        ["White", 68.8]
      ],
      ageBreakDown: [
        ["< 9", 613186],
        ["10-19", 663126],
        ["20-29", 646356],
        ["30-39", 606318],
        [">40", 2250750]
      ],
      sexBreakDown: [["Female", 30.4], ["Male", 69.6]],
      ethnicBreakDown: [
        ["Asian", 52937],
        ["Black", 1244437],
        ["Hawaii", 1976],
        ["Hispanic", 185602],
        ["Indian", 25907],
        ["Other", 64475],
        ["White", 3204402]
      ],
      feedbackFromDJs: null,
      trackRatings: null
    }

    this.setActiveTab = this.setActiveTab.bind(this);
    this.setViewMode = this.setViewMode.bind(this);
  }

  trackId = null;
  selectedCountry = null;
  selectedCity = null;

  componentWillReceiveProps(nextProps) {
    if(
      (!this.trackId && nextProps.trackId) ||
      (this.trackId !== nextProps.trackId)
    ) {
      this.trackId = nextProps.trackId;
      this.setState({ trackId: nextProps.trackId });
      // setTimeout(() => {
      //   this.getData();
      //   document.getElementById('country-charts').scrollIntoView();
      // }, 100);
    }

    if(
      (!this.selectedCountry && nextProps.selectedCountry) ||
      (this.selectedCountry !== nextProps.selectedCountry)
    ) {
      this.selectedCountry = nextProps.selectedCountry;
      this.setState({ selectedCountry: nextProps.selectedCountry });
    }

    if(
      (!this.selectedCity && nextProps.selectedCity) ||
      (this.selectedCity !== nextProps.selectedCity)
    ) {
      this.selectedCity = nextProps.selectedCity;
      this.setState({ selectedCity: nextProps.selectedCity });
    }
  }

  getChartData(state) {
    httpClient.get(`/api/dashboard/demographics/state/${state}`).then(res => {
      if(res.data.success === true) {
        const data = res.data.data;
        this.setState({
          businessOwnership: data.business,
          ageBreakDown: data.ageBreak,
          sexBreakDown: data.sexBreak,
          ethnicBreakDown: data.ethnic,
        });
      }
    });
  }

  setActiveTab(tab) {
    this.setState({
      activeTab: tab
    });
  }

  setViewMode(viewMode) {
    this.setState({
      viewMode: viewMode
    });
  }

  render() {
    return (
      <section id="city-feedback" className={this.state.selectedCity ? '' : 'hidden'}>
        <div className="container">
          <div className="text-center"><h4 className="title-outline">World - {this.state.selectedCountry} - {this.state.selectedCity}</h4></div>
          <div className="row justify-content-md-center">
            <div className="col-md-10">
              <ul className="feedback-tabs city-feedback-tabs">
                <li className={this.state.activeTab === 1 ? 'active' : null}>
                  <a onClick={(e) => this.setActiveTab(1)}>
                    <img src={BusinessOwnershipIcon} width="22" height="22" alt="Business Ownership" /> 
                    Business Ownership
                  </a>
                </li>
                <li className={this.state.activeTab === 2 ? 'active' : null}>
                  <a onClick={(e) => this.setActiveTab(2)}>
                    <img src={AgeBreakDownIcon} width="22" height="22" alt="Age Break Down" /> 
                    Age Break Down
                  </a>
                </li>
                <li className={this.state.activeTab === 3 ? 'active' : null}>
                  <a onClick={(e) => this.setActiveTab(3)}>
                    <img src={SexBreakDownIcon} width="22" height="22" alt="Sex Break Down" /> 
                    Sex Break Down
                  </a>
                </li>
                <li className={this.state.activeTab === 4 ? 'active' : null}>
                  <a onClick={(e) => this.setActiveTab(4)}>
                    <img src={EthnicBreakDownIcon} width="22" height="22" alt="Ethnic Break Down" /> 
                    Ethnic Break Down
                  </a>
                </li>
                <li className={this.state.activeTab === 5 ? 'active' : null}>
                  <a onClick={(e) => this.setActiveTab(5)}>
                    <img src={StarsIcon} width="36" height="22" alt="Feedback from DJs" style={{left: '16px'}} /> 
                    Feedback from DJs
                  </a>
                </li>
                <li className={this.state.activeTab === 6 ? 'active' : null}>
                  <a onClick={(e) => this.setActiveTab(6)}>
                    <img src={MusicIcon} width="22" height="22" alt="Track Ratings" /> 
                    Track Ratings
                  </a>
                </li>
              </ul>

              <div className="feedback-card">
                <ul className="feedback-switch">
                  <li className={this.state.viewMode === 'mapView' ? 'active' : ''}><a onClick={(e) => this.setViewMode('mapView')}>Map View</a></li>
                  <li className={this.state.viewMode === 'textView' ? 'active' : ''}><a onClick={(e) => this.setViewMode('textView')}>Text View</a></li>
                </ul>
                
                <div className={this.state.activeTab === 1 ? 'py-4' : 'hidden'}>
                  <BusinessOwnership viewMode={this.state.viewMode} data={this.state.businessOwnership} />
                </div>
                <div className={this.state.activeTab === 2 ? 'py-4' : 'hidden'}>
                  <AgeBreakDown viewMode={this.state.viewMode} data={this.state.ageBreakDown} />
                </div>
                <div className={this.state.activeTab === 3 ? 'py-4' : 'hidden'}>
                  <SexBreakDown viewMode={this.state.viewMode} data={this.state.sexBreakDown} />
                </div>
                <div className={this.state.activeTab === 4 ? 'py-4' : 'hidden'}>
                  <EthnicBreakDown viewMode={this.state.viewMode} data={this.state.ethnicBreakDown} />
                </div>
                <div className={this.state.activeTab === 5 ? 'py-4' : 'hidden'}>
                  <EthnicBreakDown viewMode={this.state.viewMode} data={this.state.ethnicBreakDown} />
                </div>
                <div className={this.state.activeTab === 6 ? 'py-4' : 'hidden'}>
                  <EthnicBreakDown viewMode={this.state.viewMode} data={this.state.ethnicBreakDown} />
                </div>

                <a className="feedback-close-button">Close</a>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

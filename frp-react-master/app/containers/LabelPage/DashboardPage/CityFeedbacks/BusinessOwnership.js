import React from 'react';
import { Pie } from 'react-chartjs-2';

const BusinessOwnership = (props) => {
  // const data = [["Asian",10.1],["Black",10.4],["Hawaii",0.1],["Hispanic",9.9],["Indian",0.7],["White",68.8]];

  const chartData = props.data ? {
    labels: props.data.map(d => d[0]),
    datasets: [{
      data: props.data.map(d => d[1]),
      backgroundColor: [
        '#F57872',
        '#032CA6',
        '#85BFF2',
        '#F2C84B',
        '#F29F05',
        '#222E73',
      ],
      hoverBackgroundColor: [
        '#F57872',
        '#032CA6',
        '#85BFF2',
        '#F2C84B',
        '#F29F05',
        '#222E73',
      ]
    }]
  } : {};

  const options = {
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          return (value).toFixed(2)+"%";
        },
        color: '#fff',
      }
    }
  }

  const legend = {
    "display": true,
    "position": "right"
  };

  const tableViewItems = () => {
    if(!props.data) {
      return null;
    }

    let table = []

    for (let i = 0; i < props.data.length; i++) {
      const item = props.data[i];
      table.push(
        <tr key={i}>
          <td>{item[0]}</td>
          <td>{item[1]}</td>
        </tr>
      )
    }
    return table
  }

  return props.data ? (
    <div>
      <div className={props.viewMode === 'mapView' ? '' : 'hidden'}>
        <Pie data={chartData} options={options} legend={legend} width={890} height={350} />
      </div>

      <div className={props.viewMode === 'textView' ? 'table-responsive' : 'hidden'} style={{maxHeight: '350px'}}>
        <table className="table table-sm table-bordered table-striped sticky-header">
          <tbody>
            {tableViewItems()}
          </tbody>
        </table>
      </div>
    </div>
  ) : <div>Please wait...</div>;
}

export default BusinessOwnership;

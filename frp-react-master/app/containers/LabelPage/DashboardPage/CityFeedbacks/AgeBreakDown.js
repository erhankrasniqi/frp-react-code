import React, { useState, useEffect } from 'react'
import { Pie } from 'react-chartjs-2';

export default function AgeBreakDown(props) {

  const [data] = useState(props.data);
  
  // const data = [["< 9",613186],["10-19",663126],["20-29",646356],["30-39",606318],[">40",2250750]];

  const chartData = data ? {
    labels: data.map(d => d[0]),
    datasets: [{
      data: data.map(d => d[1]),
      backgroundColor: [
        '#032CA6',
        '#85BFF2',
        '#F2C84B',
        '#F29F05',
        '#222E73',
      ],
      hoverBackgroundColor: [
        '#032CA6',
        '#85BFF2',
        '#F2C84B',
        '#F29F05',
        '#222E73',
      ]
    }]
  } : {};

  const options = {
    plugins: {
      datalabels: {
        color: '#fff',
      }
    }
  }

  const legend = {
    "display": true,
    "position": "right"
  };

  const tableViewItems = () => {
    if(!data) {
      return null;
    }

    let table = []

    for (let i = 0; i < data.length; i++) {
      const item = data[i];
      table.push(
        <tr key={i}>
          <td>{item[0]}</td>
          <td>{item[1]}</td>
        </tr>
      )
    }
    return table
  }

  return data ? (
    <div>
      <div className={props.viewMode === 'mapView' ? '' : 'hidden'}>
        <Pie data={chartData} options={options} legend={legend} width={890} height={350} />
      </div>

      <div className={props.viewMode === 'textView' ? 'table-responsive' : 'hidden'} style={{maxHeight: '350px'}}>
        <table className="table table-sm table-bordered table-striped sticky-header">
          <tbody>
            {tableViewItems()}
          </tbody>
        </table>
      </div>
    </div>
  ) : <div>Please wait...</div>;
}

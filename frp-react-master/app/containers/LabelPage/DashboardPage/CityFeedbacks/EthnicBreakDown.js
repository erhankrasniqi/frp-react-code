import React from 'react'
import { Pie } from 'react-chartjs-2';

export default function EthnicBreakDown(props) {

  const data = [["Asian",52937],["Black",1244437],["Hawaii",1976],["Hispanic",185602],["Indian",25907],["Other",64475],["White",3204402]];

  const chartData = {
    labels: data.map(d => d[0]),
    datasets: [{
      data: data.map(d => d[1]),
      backgroundColor: [
        '#F57872',
        '#032CA6',
        '#85BFF2',
        '#F2C84B',
        '#F29F05',
        '#222E73',
        '#85BFF2',
      ],
      hoverBackgroundColor: [
        '#F57872',
        '#032CA6',
        '#85BFF2',
        '#F2C84B',
        '#F29F05',
        '#222E73',
        '#85BFF2',
      ]
    }]
  };

  const options = {
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          let sum = 0;
          let dataArr = ctx.chart.data.datasets[0].data;
          dataArr.map(data => {
              sum += data;
          });
          let percentage = (value*100 / sum).toFixed(2)+"%";
          return percentage;
        },
        color: '#fff',
      }
    }
  }

  const legend = {
    "display": true,
    "position": "right"
  };

  const tableViewItems = () => {
    if(!data) {
      return null;
    }

    let table = []

    for (let i = 0; i < data.length; i++) {
      const item = data[i];
      table.push(
        <tr key={i}>
          <td>{item[0]}</td>
          <td>{item[1]}</td>
        </tr>
      )
    }
    return table
  }

  return (
    <div>
      <div className={props.viewMode === 'mapView' ? '' : 'hidden'}>
        <Pie data={chartData} options={options} legend={legend} width={890} height={350} />
      </div>

      <div className={props.viewMode === 'textView' ? 'table-responsive' : 'hidden'} style={{maxHeight: '350px'}}>
        <table className="table table-sm table-bordered table-striped sticky-header">
          <tbody>
            {tableViewItems()}
          </tbody>
        </table>
      </div>
    </div>
  );
}

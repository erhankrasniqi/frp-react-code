import React, { Component } from 'react'

export default class TrendingPage extends Component {
  render() {
    return (
      <div className="container">
        <div className="label-bg trending-bg"></div>
        <div className="row justify-content-md-center mt-5">
          <div className="col-md-8">
            <h4 className="text-center text-white mb-5">Comparison Of Artist & Song</h4>
            <form>
              <div className="form-group text-center">
                <div className="custom-control custom-radio custom-control-inline mr-4">
                  <input type="radio" id="customRadioInline1" name="customRadioInline1" value="1" className="custom-control-input" />
                  <label className="custom-control-label text-light" htmlFor="customRadioInline1" style={{fontSize: '12px', lineHeight: '24px'}}>Artist</label>
                </div>
                <div className="custom-control custom-radio custom-control-inline ml-2">
                  <input type="radio" id="customRadioInline2" name="customRadioInline1" value="2" className="custom-control-input" />
                  <label className="custom-control-label text-light" htmlFor="customRadioInline1" style={{fontSize: '12px', lineHeight: '24px'}}>Song</label>
                </div>

                <div className="panel-dark p-5 mt-3">
                  <div className="row">
                    <div className="col-md-6 round-outline-control">
                      <input type="text" id="main_artist" name="main_artist" className="form-control" required="required" placeholder="Start date" />
                    </div>
                    <div className="col-md-6 round-outline-control">
                      <input type="text" id="main_artist" name="main_artist" className="form-control" required="required" placeholder="End date" />
                    </div>
                  </div>
                </div>

                <div className="panel-dark p-5 mt-4">
                  <div className="row">
                    <div className="col-md-6 round-outline-control">
                      <input type="text" id="main_artist" name="main_artist" className="form-control" required="required" placeholder="First artist name" />
                    </div>
                    <div className="col-md-6 round-outline-control">
                      <input type="text" id="main_artist" name="main_artist" className="form-control" required="required" placeholder="Second artist name" />
                    </div>
                  </div>
                </div>

                <div className="buttons round-buttons text-center mb-4 mt-4">
                  <button type="button" className="btn btn-primary mr-4">Compare</button>
                  <button type="reset" className="btn btn-light">Clear</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        
      </div>
    )
  }
}

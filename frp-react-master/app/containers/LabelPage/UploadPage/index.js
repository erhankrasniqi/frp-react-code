import React, { Component } from 'react'
import LabelUploadsHistory from './LabelUploadsHistory';
import UploadFiles from './UploadFiles';
import SubscriptionPlans from './SubscriptionPlans';
import tokenProvider from '../../../utils/providers/tokenProvider';

const txtFieldState = {
  value: "",
  valid: true,
  typeMismatch: false,
  errMsg: "" //this is where our error message gets across,
};

const ErrorValidationLabel = ({ txtLbl }) => (
  <label htmlFor="" className="control-label error pr-4 mt-2 float-right">
    {txtLbl}
  </label>
);

export default class UploadPage extends Component {
  constructor(props) {
    super(props);
    this.uploadFilesComp = React.createRef();
  }

  state = {
    category: {
      ...txtFieldState,
      fieldName: "Category",
      required: true,
      requiredTxt: "Category is required"
    },
    main_artist: {
      ...txtFieldState,
      fieldName: "Main Artist",
      required: true,
      requiredTxt: "Main Artist is required",
      formatErrorTxt: "Quotes are not allowed"
    },
    feat_artist: {
      ...txtFieldState,
      fieldName: "Featuring Artist",
      required: false,
      formatErrorTxt: "Quotes are not allowed"
    },
    name: {
      ...txtFieldState,
      fieldName: "Track Name",
      required: true,
      requiredTxt: "Track Name is required",
      formatErrorTxt: "Quotes are not allowed"
    },
    files: {
      ...txtFieldState,
      fieldName: "Files",
      required: false
    },
    allFieldsValid: false,
    addedFiles: null,
    showSubscriptionPlans: false,
    user: null,
    subscription: null
  };


  reduceFormValues = formElements => {
    const arrElements = Array.prototype.slice.call(formElements); //we convert elements/inputs into an array found inside form element

    //we need to extract specific properties in Constraint Validation API using this code snippet
    const formValues = arrElements
      .filter(elem => elem.name.length > 0)
      .map(x => {
        const { typeMismatch } = x.validity;
        const { name, type, value } = x;

        return {
          name,
          type,
          typeMismatch, //we use typeMismatch when format is incorrect(e.g. incorrect email)
          value,
          valid: x.checkValidity()
        };
      })
      .reduce((acc, currVal) => {
        //then we finally use reduce, ready to put it in our state
        const { value, type } = currVal;
        let { typeMismatch, valid } = currVal;

        if (value && valid) {
          typeMismatch = !(/^[^<>'"]+$/.test(value));
          if(typeMismatch) {
            valid = !valid;
          }
        }


        const { fieldName, requiredTxt, formatErrorTxt } = this.state[
          currVal.name
        ]; //get the rest of properties inside the state object

        //we'll need to map these properties back to state so we use reducer...
        acc[currVal.name] = {
          value,
          valid,
          typeMismatch,
          fieldName,
          requiredTxt,
          formatErrorTxt
        };
        return acc;
      }, {});

    return formValues;
  };

  checkAllFieldsValid = formValues => {
    return !Object.keys(formValues)
      .map(x => formValues[x])
      .some(field => !field.valid);
  };

  onSubmit = e => {
    e.preventDefault();
    const form = e.target;

    //we need to extract specific properties in Constraint Validation API using this code snippet
    const formValues = this.reduceFormValues(form.elements);
    const allFieldsValid = this.checkAllFieldsValid(formValues);
    //note: put ajax calls here to persist the form inputs in the database.

    //END

    this.setState({ ...formValues, allFieldsValid }); //we set the state based on the extracted values from Constraint Validation API
    // console.log(formValues.map(f => f.value), 'test');
  };

  onFileAdd = e => {
    const form = document.getElementById("upload-track-form");
    const formValues = this.reduceFormValues(form.elements);
    console.log(formValues);
    const files = e.target.files;
    if(files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        const file = files[i];
        this.uploadFilesComp.current.addFile(file, formValues);
      }
    }
    document.getElementById("upload-track-form").reset();
  }

  onAddTrackClick = e => {
    const form = document.getElementById("upload-track-form");
    const formValues = this.reduceFormValues(form.elements);
    const allFieldsValid = this.checkAllFieldsValid(formValues);
    if (!allFieldsValid) {
      e.preventDefault();
    }
    this.setState({ ...formValues, allFieldsValid });
  }
  
  componentWillMount() {
    const user = tokenProvider.getUser();
    if(user) {
      this.setState({
        user: user,
        subscription: user.subscription
      });
    }
  }

  render() {
    const { category, main_artist, feat_artist, name, allFieldsValid } = this.state;
    const successFormDisplay = allFieldsValid ? "block" : "none";
    const inputFormDisplay = !allFieldsValid ? "block" : "none";

    const toggleSubscriptionPlans = async () => {
      await this.setState({showSubscriptionPlans: !this.state.showSubscriptionPlans});
    }

    const renderMainArtistValidationError = main_artist.valid ? (
      ""
    ) : (
      <ErrorValidationLabel txtLbl={main_artist.typeMismatch ? main_artist.formatErrorTxt : main_artist.requiredTxt} />
    );

    const renderFeaturingArtistValidationError = feat_artist.valid ? (
      ""
    ) : (
      <ErrorValidationLabel txtLbl={feat_artist.typeMismatch ? feat_artist.formatErrorTxt : feat_artist.requiredTxt} />
    );

    const renderTrackNameValidationError = name.valid ? (
      ""
    ) : (
      <ErrorValidationLabel txtLbl={name.typeMismatch ? name.formatErrorTxt : name.requiredTxt} />
    );

    return (
      <div className="container text-center pt-5"  style={{minHeight: '1000px'}}>
        <div className="label-bg upload-bg"></div>
        <div className="row justify-content-md-center">
          <div className="col-md-10">
            <button className="btn btn-primary" type="button" style={{fontSize: '12px', padding: '14px 29px', borderRadius: '24px'}} onClick={toggleSubscriptionPlans}>Check your subscription plan!</button>
            { this.state.showSubscriptionPlans ? <SubscriptionPlans /> : ('') }
            <p className="text-primary mt-5" style={{fontSize: '16px', fontWeight: 'bold'}}>**Important Notice**</p>
            <p className="text-white" style={{fontSize: '12px'}}>Please be aware that we only accept high quality 320k mp3 files. All tracks are screened prior to approval due to the fact that a low bit rate track can be 
              converted to a higher bit rate. Tracks that sound low in quality or audio fidelity will be refused. This include distorted or muddy sounding files caused
              by bad recording and/or mastering. It's important to convert your files to mp3s from the original studio recording or masters.</p>
            <p className="text-white" style={{fontSize: '12px'}}>Please also note that paying the fees does not translate to automatic approval/posting as it is put in place to cover the bandwidth cost incurred from 
              uploading large files. These fees are currently at it's bare minimum to cover this cost and will be decreased over time should we determine the bandwidth 
              expense is less. We strongly suggest you select the monthly plan which is the lowest fee at $4.99 and can be canceled at anytime.</p>
          </div>
        </div>

        <div className="row justify-content-md-center pb-5">
          <div className="col-md-9">
            <form
              id="upload-track-form"
              className="upload-track-form panel-dark p-3 mt-4"
              onSubmit={this.onSubmit}
              noValidate
            >
              <h4 className="text-center text-white mb-5">Upload Tracks</h4>
              <div className="row justify-content-md-center text-left">
                <div className="col-md-5 col-sm-6 pb-5 round-outline-control down-arrow">
                  <label htmlFor="category" className="control-label text-white pl-4">Category</label>
                  <select name="category" id="category" className="form-control" defaultValue="">
                    <option value="" disabled>Select Category</option>
                    <option value="1">Hip-Hop</option>
                    <option value="2">R&amp;B</option>
                    <option value="3">Reggae</option>
                    <option value="4">House</option>
                    <option value="5">Mainstream</option>
                    <option value="6">Calypso / Soca</option>
                    <option value="7">Latin</option>
                    <option value="9">Remixes</option>
                  </select>
                </div>
                <div className="col-md-5 col-sm-6 pb-5 round-outline-control">
                  <label htmlFor="main_artist" className="control-label text-white pl-4">Main Artist</label>
                  <input type="text" id="main_artist" name="main_artist" className="form-control" required="required" />
                  {renderMainArtistValidationError}
                </div>
              </div>
              <div className="row justify-content-md-center text-left">
                <div className="col-md-5 col-sm-6 pb-5 round-outline-control">
                  <label htmlFor="feat_artist" className="control-label text-white pl-4">Featuring Artist</label>
                  <input type="text" id="feat_artist" name="feat_artist" className="form-control" />
                  {renderFeaturingArtistValidationError}
                </div>
                <div className="col-md-5 col-sm-6 pb-5 round-outline-control">
                  <label htmlFor="name" className="control-label text-white pl-4">Track Name</label>
                  <input type="text" id="name" name="name" className="form-control" required="required" />
                  {renderTrackNameValidationError}
                </div>
              </div>
              <input id="fileInput" type="file" title="Add tracks" name="files" multiple="" accept="audio/mpeg" onChange={this.onFileAdd} style={{display: 'none'}} />

              <div className="buttons round-buttons text-center mb-4">
                <label htmlFor="fileInput" onClick={this.onAddTrackClick} className="btn btn-primary mr-4 mb-0">Add tracks</label>
                <button type="submit" className="btn btn-light mr-4">Upload All</button>
                <button type="reset" className="btn btn-light">Clear All</button>
              </div>

            </form>
            
            <UploadFiles ref={this.uploadFilesComp} subscription={this.state.subscription} />
          
          </div>
        </div>


        <LabelUploadsHistory />
      </div>
    )
  }
}

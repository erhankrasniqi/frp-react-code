import React, { Component } from 'react'
import PaypalButton from '../../../images/paypal-btn.png';
import ExclamationIcon from '../../../images/exclamation-icon.svg';
import CCInfoDark from '../../../images/credit-card-info-dark.png';
import CCInfoLight from '../../../images/credit-card-info-light.png';
import httpClient from '../../../utils/providers/httpClient';
import tokenProvider from '../../../utils/providers/tokenProvider';

export default class SubscriptionPayment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentMethod: null //'credit-card' // null
    }

    this.selectPaymentMethod = this.selectPaymentMethod.bind(this);
    this.onCreditCardSubmit = this.onCreditCardSubmit.bind(this);
  }

  selectPaymentMethod = (event) => {
    const paymentMethod = event.target.getAttribute('data-method');
    this.setState({paymentMethod: paymentMethod});
  }

  onCreditCardSubmit = (event) => {
    event.preventDefault();

    const plan = {
      period: this.props.plan.id < 4 ? 'month' : 'year',
      periodNumber: this.props.plan.id === 2 ? 3 : this.props.plan.id === 3 ? 6 : 1,
      price: this.props.plan.price
    };

    const data = {
      plan: plan,
      first_name: event.target.first_name.value,
      last_name: event.target.last_name.value,
      address: event.target.address.value,
      city: event.target.city.value,
      state: event.target.state.value,
      zip: event.target.zip.value,
      country: event.target.country.value,
      email: event.target.email.value,
      phone: event.target.phone.value,
      credit_card_type: event.target.credit_card_type.options[event.target.credit_card_type.options.selectedIndex].value,
      card_number: event.target.card_number.value,
      exp: event.target.exp.value,
      ccid: event.target.ccid.value,
      comment: event.target.comment.value,
      terms: event.target.terms.checked,
    }

    if(!data.terms) {
      alert('Please accept terms and conditions');
      return false;
    }

    var regex = new RegExp("^[0-9]{16}$");
    if (!regex.test(data.card_number)) {
      alert('Please enter valid credit card number!');
      return false;
    }

    httpClient.post('api/subscription', data).then(res => {
      if(res.data.success) {
        const user = tokenProvider.getUser();
        user.subscription = {
          period: plan.period,
          periodNumber: plan.periodNumber
        }
        tokenProvider.setUser(user);
        alert('Payment has been completed successfully');
        this.setState({
          paymentMethod: null
        })
        this.props.onSuccessPayment();
      } else {
        alert('Something went wrong! Please try again later.');
      }
    });
  }

  render() {
    return (
      <div>
        <h4 className="text-center text-white mb-5">Choose payment method</h4>
        <div className="panel-dark p-2 p-sm-5">
          <div className="row justify-content-md-center">
            <div className="col-md-10">
              <p className="text-left text-white mb-5" style={{fontSize: '12px'}}>
                Please provide us with your payment info below over our 100% secure gateway to complete 
                the registration process. Our "SSL Security & Authentic Site Certificate" can be found at 
                the bottom right hand corner of this and all pages. Please take the time to verify.
              </p>
              <div className="buttons round-buttons text-center mb-4">
                <button type="button" className={'btn mr-4 ' + (this.state.paymentMethod === 'credit-card' ? 'btn-primary' : 'btn-light')} onClick={this.selectPaymentMethod} data-method="credit-card">Credit Card</button>
                <button type="button" className={'btn ' + (this.state.paymentMethod === 'paypal' ? 'btn-primary' : 'btn-light')} onClick={this.selectPaymentMethod} data-method="paypal">PayPal</button>
              </div>
              {
                this.state.paymentMethod === 'credit-card' ? 
                  <div>
                    <h5 className="text-center text-white mt-5">Payment Information</h5>
                    <form method="POST" className="text-left" onSubmit={this.onCreditCardSubmit}>
                      <div className="row round-outline-control">
                        <div className="col-md-6 form-group">
                          <label htmlFor="first_name" className="control-label text-white pl-4">First Name</label>
                          <input type="text" id="first_name" name="first_name" className="form-control" required="required" />
                        </div>
                        <div className="col-md-6 form-group">
                          <label htmlFor="last_name" className="control-label text-white pl-4">Last Name</label>
                          <input type="text" id="last_name" name="last_name" className="form-control" required="required" />
                        </div>
                        <div className="col-md-6 form-group">
                          <label htmlFor="address" className="control-label text-white pl-4">Address</label>
                          <input type="text" id="address" name="address" className="form-control" required="required" />
                        </div>
                        <div className="col-md-6 form-group">
                          <label htmlFor="city" className="control-label text-white pl-4">City</label>
                          <input type="text" id="city" name="city" className="form-control" required="required" />
                        </div>
                        <div className="col-md-6 form-group">
                          <label htmlFor="state" className="control-label text-white pl-4">State</label>
                          <input type="text" id="state" name="state" className="form-control" required="required" />
                        </div>
                        <div className="col-md-6 form-group">
                          <label htmlFor="zip" className="control-label text-white pl-4">Zip</label>
                          <input type="text" id="zip" name="zip" className="form-control" required="required" />
                        </div>
                        <div className="col-md-6 form-group">
                          <label htmlFor="country" className="control-label text-white pl-4">Country</label>
                          <input type="text" id="country" name="country" className="form-control" required="required" />
                        </div>
                        <div className="col-md-6 form-group">
                          <label htmlFor="email" className="control-label text-white pl-4">Email</label>
                          <input type="email" id="email" name="email" className="form-control" required="required" />
                        </div>
                        <div className="col-md-6 form-group">
                          <label htmlFor="phone" className="control-label text-white pl-4">Phone</label>
                          <input type="phone" id="phone" name="phone" className="form-control" required="required" />
                        </div>
                      </div>
                      <div className="row round-outline-control mt-5">
                        <div className="col-md-6 form-group round-outline-control down-arrow">
                          <label htmlFor="credit_card_type" className="control-label text-white pl-4">Credit Card Types</label>
                          <select name="credit_card_type" id="credit_card_type" className="form-control" defaultValue="">
                            <option value="" disabled></option>
                            <option value="visa">Visa</option>
                            <option value="mastercard">MasterCard</option>
                            <option value="discover">Discover</option>
                            <option value="american-express">American Express</option>
                          </select>
                        </div>
                        <div className="col-md-6 form-group">
                          <label htmlFor="card_number" className="control-label text-white pl-4">Card Number</label>
                          <input type="text" id="card_number" name="card_number" className="form-control" required="required" />
                        </div>
                        <div className="col-md-6 form-group">
                          <label htmlFor="exp" className="control-label text-white pl-4">EXP</label>
                          <input type="text" id="exp" name="exp" className="form-control" required="required" />
                        </div>
                        <div className="col-md-6 form-group">
                          <label htmlFor="ccid" className="control-label text-white pl-4">CCID</label>
                          <input type="text" id="ccid" name="ccid" className="form-control" required="required" />
                        </div>
                        <div className="col-md-12 form-group">
                          <label htmlFor="comment" className="control-label text-white pl-4">Comment</label>
                          <textarea id="comment" style={{minHeight: '90px'}} name="comment" className="form-control" required="required"></textarea>
                        </div>
                      </div>
                      <div className="custom-control custom-checkbox mx-4">
                        <input className="custom-control-input" type="checkbox" value="" id="terms" name="terms" />
                        <label className="custom-control-label text-white" htmlFor="terms" style={{fontSize: '12px', fontWeight: 'bold'}}>
                          By submitting your info, you agree to Franchise Record Pool's <a href="#">Terms of Use</a>. You also 
                          confirm that the information you submit on this form is correct and solely yours.
                        </label>
                      </div>
                      <p className="p-4 text-white" style={{fontSize: '12px', fontWeight: 'bold'}}>
                        <img src={ExclamationIcon} alt="exclamation-icon" width="16" height="22" className="mr-2 mb-5 float-left" />
                        For your protection, we ask that you enter an extra 3-4 digit number called the CCID. 
                        The CCID is NOT your PIN number. It is an extra ID printed on your Visa, MasterCard, 
                        Discover, or American Express Card.
                      </p>
                    
                      <img src={CCInfoDark} alt="Credit Card Information" className="hide-dark img-fluid mx-auto" />
                      <img src={CCInfoLight} alt="Credit Card Information" className="hide-light img-fluid mx-auto" />

                      <div className="row mt-5 pt-4">
                        <div className="col-sm-6 text-left pt-1"><h4 className="text-white font-weight-normal">Total <span className="font-weight-bold text-primary">$ {this.props.plan.price}</span></h4></div>
                        <div className="col-sm-6 buttons round-buttons  text-right">
                          <button type="reset" className="btn btn-light mr-4">Cancel</button>
                          <button type="submit" className="btn btn-primary">Send</button>
                        </div>
                      </div>
                    </form>
                  </div>
                : this.state.paymentMethod === 'paypal' ? 
                  <div className="row mt-5 pt-4">
                    <div className="col-sm-6 text-left pt-2"><h4 className="text-white font-weight-normal">Total <span className="font-weight-bold text-primary">$ {this.props.plan.price}</span></h4></div>
                    <div className="col-sm-6 buttons round-buttons  text-right">
                      <a href="https://www.paypal.com/" target="_blank"><img src={PaypalButton} alt="Check out with PayPal" /></a>
                    </div>
                  </div>
                :
                ('')
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

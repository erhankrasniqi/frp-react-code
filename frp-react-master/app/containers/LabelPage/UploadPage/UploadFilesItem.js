import React, { Component } from 'react';
import httpClient from '../../../utils/providers/httpClient';
import axios from "axios";

export default class UploadFilesItem extends Component {
  constructor(props) {
    super(props);
    this.onUpload = this.onUpload.bind(this);
    this.onCancel = this.onCancel.bind(this);

    this.fileUpload = this.fileUpload.bind(this)
  }
  state = {
    uploading: false,
    uploaded: false,
    uploadPercentage: 0,
    uploadBytes: 0,
    source: null
  }

  totalBytes = 0;
  uploadBytes = 0;
  uploadPercentage = 0;

  
  componentDidMount() {
    this.totalBytes = this.props.item.file.size;
    const CancelToken = axios.CancelToken;
    this.setState({source: CancelToken.source()});
  }
  
  onUpload = () => {
    if(!this.props.subscription) {
      alert(`You don't have any active subscription plan.`);
      return;
    }

    this.setState({
      uploaded: false,
      uploading: true
    })
    this.fileUpload(this.props.item).then((response)=>{
      console.log(response.data);
      if(response.data.success === true) {
        this.setState({
          uploaded: true,
          uploading: false
        })
        this.props.onUpload(this.props.item);
      } else {
        this.setState({
          uploaded: false,
          uploading: false
        })
        alert(response.data.message);
      }
    })
    // this.updateProgress();
  }

  onCancel = () => {
    this.state.source.cancel('Operation canceled by the user.');
    this.props.onRemove();
  }
  
  fileUpload(item) {
    const formData = new FormData();
    formData.append('tracks', item.file);
    formData.append('category', item.formValues.category.value);
    formData.append('feat_artist', item.formValues.feat_artist.value);
    formData.append('main_artist', item.formValues.main_artist.value);
    formData.append('name', item.formValues.name.value);
    const e = document.getElementById("version_" + this.props.index);
    formData.append('version', e.options[e.selectedIndex].value);

    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      },
      onUploadProgress: async progressEvent => {
        // console.log(progressEvent.loaded)
        await this.setState({
          uploadPercentage: ((progressEvent.loaded / this.totalBytes) * 100 ),
          uploadBytes: progressEvent.loaded
        })
      },
      cancelToken: this.state.source.token
    }

    return httpClient.post(`/api/track`, formData, config)
  }
  
  // updateProgress() {
  //   let interval;
  //   const addValue = Math.round(this.totalBytes / 100);

  //   interval = setInterval(() => {
  //     if(this.uploadPercentage < 100) {
  //       this.uploadPercentage += 1;
  //       this.uploadBytes += addValue;
  //     } else  {
  //       this.setState({
  //         uploaded: true,
  //         uploading: false
  //       })
  //       clearInterval(interval);

  //       this.props.onUpload(this.props.item);
  //     }
  //     this.setState({
  //       uploadPercentage: this.uploadPercentage,
  //       uploadBytes: this.uploadBytes
  //     })
  //   }, 100)
  // }

  render() {
    return this.props.item ? (
      <tr>
        <td style={{verticalAlign: 'middle'}} className="text-left">{this.props.item.fileName}</td>
        <td style={{verticalAlign: 'middle'}}>{this.props.item.size}</td>
        <td style={{verticalAlign: 'middle'}}>
          {this.state.uploaded ? (
            <span>Track Successfully Upload</span>
          ): (
            <div className="progress">
              <div className="progress-bar" role="progressbar" style={{width: this.state.uploadPercentage + '%'}}></div>
            </div>
          )}
        </td>
        <td>
          <select name={"version_" + this.props.index} id={"version_" + this.props.index} className="form-control form-control-sm" defaultValue="0" disabled={this.state.uploading || this.state.uploaded}>
            <option value="0" disabled>Select Version</option>
            <option value="1">Clean</option>
            <option value="2">Dirty</option>
            <option value="3">Instrumental</option>
            <option value="11">Acapella Clean</option>
            <option value="12">Acapella Dirty</option>
            <option value="13">Intro Clean</option>
            <option value="14">Intro Dirty</option>
            <option value="15">Original</option>
            <option value="16">Main</option>
            <option value="17">Radio Edit</option>
            <option value="18">Extended</option>
          </select>
        </td>
        <td>
          {this.state.uploaded ? null : (<button type="button" onClick={this.onUpload} className="btn btn-primary btn-sm" disabled={this.state.uploading}>Upload</button>)}
        </td>
        <td>
          {this.state.uploaded ? null : (<button type="button" onClick={this.onCancel} className="btn btn-danger btn-sm">Cancel</button>)}
        </td>
      </tr>
    )
    : (<tr></tr>);
  }
}

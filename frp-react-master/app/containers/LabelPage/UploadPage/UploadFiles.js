import React, { Component } from 'react'
import UploadFilesItem from './UploadFilesItem';

export default class UploadFiles extends Component {
  constructor(props) {
    super(props);
    this.addFile = this.addFile.bind(this);
    this.onUpload = this.onUpload.bind(this);
  }

  state = {
    addedFiles: null
  }

  bytesToSize(bytes,decimals) {
    if(bytes == 0) return '0 Bytes';
    var k = 1024,
        dm = decimals <= 0 ? 0 : decimals || 2,
        sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
        i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  addFile = (file, formValues) => {
    if(!file) return;

    const addedFiles = this.state.addedFiles ? this.state.addedFiles : [];

    addedFiles.push({
      file,
      fileName: file.name,
      size: this.bytesToSize(file.size),
      version: null,
      uploadProgress: 0,
      formValues: formValues
    });

    this.setState({
      addedFiles: addedFiles
    });
  }

  onRemove = (index) => {
    const addedFiles = this.state.addedFiles.splice(index, -1);
    this.setState({
      addedFiles: addedFiles.length > 0 ? addedFiles : null
    })
  }

  onUpload = (index) => {
    console.log(index);
  }
  

  render() {
    const items = () => {
      if(!this.state.addedFiles) {
        return null;
      }
  
      let table = []
  
      for (let i = 0; i < this.state.addedFiles.length; i++) {
        const item = this.state.addedFiles[i];
        table.push(
          <UploadFilesItem key={i} item={item} index={i} onRemove={this.onRemove} onUpload={this.onUpload} subscription={this.props.subscription} />
        )
      }
      return table
    }

    return this.state.addedFiles ? (
      <div className="mt-4">
        <table className="table table-bordered frp-table text-white">
          <thead>
            <tr>
              <th>Track Name</th>
              <th>Size</th>
              <th>Progress</th>
              <th>Select Version</th>
              <th>Upload</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {items()}  
          </tbody>
        </table>
      </div>
    ) : (<div></div>)
  }
}

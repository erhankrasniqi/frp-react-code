import React, { Component } from 'react';
import paginate from '../../../utils/paginate';
import Moment from 'moment'
import httpClient from '../../../utils/providers/httpClient';

export default class LabelUploadsHistory extends Component {
  state = {
    orderBy: 'uploaded',
    orderDir: 'ASC',
    page: 1,
    pageSize: 10,
    totalItems: 0,
    items: [],
    paginate: null,
  }

  onPageChange = async (e) => {
    e.preventDefault();
    const page = e.target.dataset.page;
    if(page === this.state.page) {
      return;
    }
    const loop = Math.ceil(this.state.records / this.state.total);
    if(page === 0 || page > loop) {
      return;
    }
    await this.setState({page: parseInt(page)});
    this.getUploadHistory();
  }

  onItemsPerPageChange = async (e) => {
    const value = e.target.options[e.target.selectedIndex].value;
    await this.setState({
      pageSize: parseInt(value)
    });
    this.getUploadHistory();
  }

  getUploadHistory = () => {
    httpClient.get(`/api/track/list/page/${this.state.page}/size/${this.state.pageSize}`)
    .then((res) => {
      if(res.data.success === true) {
        this.setState({items: res.data.data.items, totalItems: res.data.data.totalItems });
        this.generatePagination();
      }
    })
  }
  async componentWillMount() {
    this.getUploadHistory();
  }

  async generatePagination() {
    const generatedPaginate = paginate(this.state.totalItems, this.state.page, this.state.pageSize, 5);
    await this.setState({
      paginate: generatedPaginate
    })
    console.log(generatedPaginate);
  }

  async updateOrderBy(event, column) {
    event.preventDefault();

    if(this.state.orderBy !== column) {
      await this.setState({
        orderBy: column
      });
    } else {
      await this.setState({
        orderDir: this.state.orderDir === 'ASC' ? 'DESC' : 'ASC'
      });
    }

    console.log({
      orderBy: this.state.orderBy,
      orderDir: this.state.orderDir
    });
  }

  render() {
    Moment.locale('tr');
    const items = () => {
      if(!this.state.items) {
        return null;
      }
  
      let table = []
      this.state.items.forEach((item, i) => {
        const formattedDT = Moment(item.created).format('MM/DD/YYYY')
        table.push(
          <tr key={item.id}>
            <td width="50">{item.id}</td>
            <td width="170">
              {formattedDT}
            </td>
            <td>{item.name}</td>
            <td>{item.status}</td>
            <td></td>
            {/* {item.map((cell, index) => (
              <td key={index}>{cell}</td>
            ))} */}
          </tr>        
        )
      });
  
      // for (let i = 0; i < this.state.rows.length; i++) {
      //   const item = this.state.rows[i].cell;
      //   table.push(
      //     <tr key={i}>
      //       {item.map((cell, index) => (
      //         <td key={index}>{cell}</td>
      //       ))}
      //     </tr>
      //   )
      // }
      return table
    }
  

    const nagiation = () => {
      if(!this.state.paginate) {
        return '';
      }
      const currentPage = this.state.page;
      let pages = [];
      for (let i = 0; i < this.state.paginate.pages.length; i++) {
        const page = this.state.paginate.pages[i];
        pages.push(
          <li key={i} className={page == currentPage ? "page-item active" : "page-item"}>
            <a className="page-link" href="#" data-page={page} onClick={this.onPageChange}>{page}</a>
          </li>
        )
      }
      console.log(pages);
      return pages;
    }

    const getOrderDir = (column) => {
      if(this.state.orderBy !== column) {
        return null;
      }

      return this.state.orderDir === 'ASC' ? (<i className="fas fa-sort-up"></i>) : (<i className="fas fa-sort-down"></i>);
    }
  
    return (
      <div className="mb-5" style={{position: 'relative', zIndex: '2'}}>
        <h4 className="text-center text-white mb-5">Label Uploads History</h4>
        <div className="table-responsive">
          <table className="table table-bordered frp-table text-white">
            <thead>
              <tr>
                <th className={this.state.orderBy === 'uploaded' ? 'bg-primary' : ''} colSpan="2"><a href="#" onClick={(e) => { this.updateOrderBy(e, 'uploaded') }}>Uploaded {getOrderDir('uploaded')}</a></th>
                <th className={this.state.orderBy === 'name' ? 'bg-primary' : ''} width="200"><a href="#" onClick={(e) => { this.updateOrderBy(e, 'name') }}>Name {getOrderDir('name')}</a></th>
                <th className={this.state.orderBy === 'status' ? 'bg-primary' : ''} width="100"><a href="#" onClick={(e) => { this.updateOrderBy(e, 'status') }}>Status {getOrderDir('status')}</a></th>
                <th width="100">Comment</th>
              </tr>
            </thead>
            <tbody>
              {items()}  
            </tbody>
          </table>
        </div>
  
        <div className="row frp-table-pagination">
          <div className="col-md-6">
            <nav aria-label="Page navigation">
              <ul className="pagination">
                {nagiation()}
              </ul>
            </nav>
          </div>
          <div className="col-md-6">
            <form className="form-inline float-right items-per-page">
              <label htmlFor="tracksPerPage">Tracks per page</label>
              <select className="form-control" id="tracksPerPage" onChange={this.onItemsPerPageChange}>
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="300">300</option>
              </select>
            </form>
          </div>
        </div>
  
      </div>
    )
  }
}

import React, { Component } from 'react'
import SubscriptionPayment from './SubscriptionPayment';
import tokenProvider from '../../../utils/providers/tokenProvider';

export default class SubscriptionPlans extends Component {
  constructor(props) {
    super(props);
    this.state = {
      plans: [
        {
          id: 1,
          label: 'Monthly',
          price: 4.99
        },
        {
          id: 2,
          label: '3 Months',
          price: 12.99
        },
        {
          id: 3,
          label: '6 Months',
          price: 24.99
        },
        {
          id: 4,
          label: 'Yearly',
          price: 49.99
        }
      ],
      activePlan: null,
      selectedPlan: null
    }
    
    this.selectPlan = this.selectPlan.bind(this);
    this.updateActivePlan = this.updateActivePlan.bind(this);
  }

  async componentDidMount() {
    // Test only
    // this.setState({selectedPlan: this.state.plans[2]});
    this.updateActivePlan();
  }

  updateActivePlan = async () => {
    const user = tokenProvider.getUser();
    if(user && user.subscription) {
      if(user.subscription.period === 'month') {
        switch(user.subscription.periodNumber) {
          case 6:
            await this.setState({activePlan: 3});
            break;
          case 3:
            await this.setState({activePlan: 2});
            break;
          default:
            await this.setState({activePlan: 1});
            break;
        }
      } else if (user.subscription.period === 'year') {
        await this.setState({activePlan: 4});
      }
    } else {
      await this.setState({activePlan: null});
    }
    
    this.setState({
      selectedPlan: null
    });
  }

  selectPlan = (event) => {
    const id = event.target.getAttribute("data-id");
    if(id <= this.state.activePlan) {
      alert('You bought this subscription');
      return;
    }

    const plans = this.state.plans.filter(p => p.id === parseInt(id));
    if(plans && plans.length > 0) {
      this.setState({selectedPlan: plans[0]})
    }
  }

  render() {


    return (
      <div className="pt-5 mt-5 subscription-plans">
        <h4 className="text-center text-white mb-5">Select a plan</h4>
        <div className="row text-white mb-5 pb-3">
          {
            this.state.plans.map(plan => (
              <div key={plan.id} className="col-md-3 mb-2">
                <button onClick={this.selectPlan} data-id={plan.id} className={'plan-button ' + (plan.id === this.state.activePlan || (this.state.selectedPlan && this.state.selectedPlan.id === plan.id) ? 'active' : '')}>
                  {plan.label}
                  <span data-id={plan.id} className="price">$ {plan.price}</span>
                </button>
              </div>
            ))
          }
        </div>
        { this.state.selectedPlan ? <SubscriptionPayment plan={this.state.selectedPlan} onSuccessPayment={this.updateActivePlan} /> : ('') }
      </div>
    )
  }
}

import React, { Component } from 'react'

export default class AssignPage extends Component {
  render() {
    return (
      <div className="mb-5 pb-5">
        <div className="label-bg assign-bg"></div>
        <div className="row justify-content-md-center mt-5">
          <div className="col-md-5">
            <h4 className="text-center text-white mb-5">Assign New Artist</h4>
            <form>
              <div className="form-group text-center">
                <div className="panel-dark p-5 mt-3">
                  <div className="round-outline-control">
                    <input type="text" id="artist_name" name="artist_name" className="form-control" required="required" placeholder="Enter artist name here " style={{textAlign: 'center'}} />
                  </div>
                  <div className="round-buttons mt-4">
                    <button type="submit" className="btn btn-primary mr-4">Add</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        
      </div>
    )
  }
}

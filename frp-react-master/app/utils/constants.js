export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';
export const LOGIN_URL = '/auth/login';
// export const SERVER_URL = 'http://192.168.0.121:3003';
export const SERVER_URL = 'http://52.29.88.152:3003';
import { combineReducers } from 'redux';
import register from './registerReducer';
import login from './loginReducer';
import { connectRouter } from 'connected-react-router'
import history from '../history';

const rootReducer = combineReducers({
  register, login,
  router: connectRouter(history),
});

export default rootReducer;

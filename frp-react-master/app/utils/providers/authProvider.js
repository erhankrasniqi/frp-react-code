
import { SERVER_URL } from '../constants';
import axios from 'axios';

export const registerUserService = (request) => {
  const REGISTER_API_ENDPOINT = `${SERVER_URL}/api/auth/register`;
  
  const parameters = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(request.user)
  };

  return fetch(REGISTER_API_ENDPOINT, parameters)
    .then(response => {
      return response.json();
    })
    .then(json => {
      return json;
    });
};

export const loginUserService = (request) => {
  const LOGIN_API_ENDPOINT = `${SERVER_URL}/api/auth/login`;

  // const req = axios.post(LOGIN_API_ENDPOINT, request.user);
  // return req;
  const parameters = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(request.user)
  };

  return fetch(LOGIN_API_ENDPOINT, parameters)
    .then(response => {
      console.log(response);
      return response.json();
    })
    .then(json => {
      return json;
    });
};

export const logoutUserService = () => {
  localStorage.removeItem('token');
  return { logout: true };
}
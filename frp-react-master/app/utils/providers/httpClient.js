import { SERVER_URL } from '../constants';
import axios from "axios";
import tokenProvider from './tokenProvider';

const httpClient = axios.create({
  baseURL: SERVER_URL,
  
})

const isHandlerEnabled = (config={}) => {
  return config.hasOwnProperty('handlerEnabled') && !config.handlerEnabled ? 
    false : true
}

const requestHandler = (request) => {
  if (isHandlerEnabled(request)) {
    // Modify request here
    request.headers['Authorization'] = `${tokenProvider.getToken()}`
  }
  return request
}

httpClient.interceptors.request.use(
  request => requestHandler(request)
)

export default httpClient;
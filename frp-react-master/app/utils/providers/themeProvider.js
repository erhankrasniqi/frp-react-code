import React from "react";
import { ThemeProvider } from 'styled-components';
const ThemeToggleContext = React.createContext();

export const useTheme = () => React.useContext(ThemeToggleContext);

export const FRPThemeProvider = ({ children }) => {

  const [themeState, setThemeState] = React.useState({
    mode: localStorage.getItem('colorTheme') || 'dark'
  });

  const storeConfig = async (mode) => {
    localStorage.setItem('colorTheme', mode);
  }

  const toggle = () => {
    const mode = (themeState.mode === 'light' ? `dark` : `light`);
    setThemeState({ mode: mode });
    storeConfig(themeState.mode === 'light' ? `dark` : `light`);
  };

  return (
    <ThemeToggleContext.Provider value={{ toggle: toggle }}>
      <ThemeProvider
        theme={{
          mode: themeState.mode
        }}
      >
          {children}
      </ThemeProvider>
    </ThemeToggleContext.Provider>
  );
};

export default ThemeProvider;
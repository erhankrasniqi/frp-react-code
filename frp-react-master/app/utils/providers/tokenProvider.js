import Cookies from 'js-cookie';
import { Subject } from 'rxjs';
import { LOGIN_URL } from '../constants';

const auth = new Subject();

const tokenProvider = {
  setToken: token => setToken(token),
  getToken: () => getToken(),
  checkToken: () => checkToken(),
  setUser: user => setUser(user),
  getUser: () => getUser(),
  checkAuth: () => auth.asObservable(),
  isAuthenticated: () => !!getToken(),
  logout: () => logout()
};

const setToken = (token) => {
  const expires = (token.expires_in || 86400) * 1000;
  const inOneHour = new Date(new Date().getTime() + expires);
  Cookies.set('access_token', token, { expires: inOneHour })
  auth.next({isAuthenticated: true});
}

const getToken = () => Cookies.get('access_token')

const checkToken = () => {
  let token = getToken();
  if (token && token !== '') {
    return token;
  } else {
    return null;
  }
}

const setUser = (user) => {
  localStorage.setItem('user', JSON.stringify(user));
}

const getUser = () => {
  const user = localStorage.getItem('user')
  return user ? JSON.parse(user) : null;
}

const logout = () => {
  Cookies.remove('access_token');
  localStorage.removeItem('user');
  auth.next({isAuthenticated: false});
  redirectToLogin(true);
}

const redirectToLogin = (logout) => {
  window.location.replace(
    `${LOGIN_URL}${!logout ? `?next=${window.location.href}` : ''}`
  )
  // history.push('/auth/login');
}

export default tokenProvider;